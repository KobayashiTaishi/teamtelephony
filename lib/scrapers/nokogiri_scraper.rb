require "open-uri"

class Scrapers::NokogiriScraper
  def self.twitter_share_count(url)
    charset = nil
      html = open("http://urls.api.twitter.com/1/urls/count.json?url=#{url}") do |f|
        charset = f.charset # 文字種別を取得
        f.read # htmlを読み込んで変数htmlに渡す
      end
      # htmlをパース(解析)してオブジェクトを作成
      doc = Nokogiri::HTML.parse(html, nil, charset)
      # jsonこんな感じになる
      # {"count"=>471, "url"=>"http://gigazine.net/news/20140925-emma-nude-hoax/"}
      json = JSON.parse(doc.xpath('//p/text()').text)
      json["count"]
  end
end
