class ContentsWebDriver
  def initialize
    @driver = Selenium::WebDriver.for :chrome
    # @driver.manage.timeouts.page_load = 10
    @wait = Selenium::WebDriver::Wait.new(timeout: 10)
    @page_count = 0
    @window_handles = []
    @window_handle = nil
    @last_find_element_option = nil
  end

  def init_driver(url)
    @driver.navigate.to url
    @driver
  end
  #optionが存在するかどうか
  def exist?(option)
    begin
      @last_find_element_option = option
      @driver.find_element(option)
    rescue
      return false
    end
    return true
  end

  def quit
    @driver.quit
  end

  #optionをクリックする
  def click(option)
    scroll_element(find(option)).click
  end

  #optionを検索する
  # def find(option)
  #   find_element(option)
  # end

  #optionを検索する
  # def find_element(option)
  #   @last_find_element_option = option
  #   element = wait_until { @driver.find_element(option) }
  #   return element
  # end

  #optionを検索する
  #@note こちらは複数のエレメントがかえってくることがある
  def finds(option)
    find_elements(option)
  end

  #optionを検索する
  #@note こちらは複数のエレメントがかえってくることがある
  def find_elements(option)
    wait_until { is_readystate_complete? }
    @last_find_element_option = option
    @driver.find_elements(option)
  end

  #真がかえってくるまでブロックの中身を実行し続ける
  def wait_until
    @wait.until { yield } rescue raise WaitError, "Cannot find an element with find_element(" + @last_find_element_option.to_s + ")."
  end

  #チェックボックスが存在するかどうか
  # @param attr 属性
  # @param val 値
  def checkbox_exist?(attr, val)
    exist?({xpath: "//input[@type=\"checkbox\" and @#{attr}=\"#{val}\"]"})
  end

  #フレームが存在するかどうか
  # @param attr 属性
  # @param val 値
  def frame_exist?(attr, val)
    exist?({xpath: "//frame[@#{attr}=\"#{val}\"]"})
  end

  #Imageタグが存在するかどうか
  # @param attr 属性
  # @param val 値
  def image_exist?(attr, val)
    exist?({xpath: "//*[@#{attr}=\"#{val}\"]"})
  end

  #Tableタグが存在するかどうか
  # @param attr 属性
  # @param val 値
  def table_exist?(attr, val)
    exist?({xpath: "//table[@#{attr}=\"#{val}\"]"})
  end

  #文字列が存在するかどうか
  # @param text 文字列
  # @param tag_name タグ名
  def text_exist?(text, tag_name=nil)
    if tag_name
      exist?({xpath: "//#{tag_name}[text()[contains(.,\"#{text}\")]]"})
    else
      exist?({xpath: "//*[text()[contains(.,\"#{text}\")]]"})
    end
  end

  #ラジオボックスが存在するかどうか
  # @param attr 属性
  # @param val 値
  # @param options 任意指定(:captalならRADIOで判定を行う)
  def radio_exist?(name, value, *options)
    if :captal.in? options
      exist?({xpath: "//input[@type=\"RADIO\" and @name=\"#{name}\" and @value=\"#{value}\"]"})
    else
      exist?({xpath: "//input[@type=\"radio\" and @name=\"#{name}\" and @value=\"#{value}\"]"})
    end
  end

  #送信ボタンが存在するかどうか
  # @param attr 属性
  # @param val 値
  def submit_exist?(name, value)
    exist?({xpath: "//input[@type=\"submit\" and @name=\"#{name}\" and @value=\"#{value}\"]"})
  end

  #ボタンが存在するかどうか
  # @param attr 属性
  # @param val 値
  def button_exist?(attr, val)
    exist?({xpath: "//input[@#{attr}=\"#{val}\"]"})
  end

  #リンクが存在するかどうか
  # @param attr 属性
  # @param val 値
  def link_exist?(attr, val)
    exist?({xpath: "//a[@#{attr}=\"#{val}\"]"})
  end

  #selectタグが存在するかどうか
  # @param attr 属性
  # @param val 値
  def select_exist?(attr, val)
    exist?({xpath: "//select[@#{attr}=\"#{val}\"]"})
  end

  #任意のタグが存在するかどうか
  # @param element タグ
  # @param attr 属性
  # @param val 値
  def element_exist?(element, attr, val)
    exist?({xpath: "//#{element}[@#{attr}=\"#{val}\"]"})
  end

  # articleをcsvにparseする
  # @param attr 属性
  # @param val 値
  def find_article(attr, val)
    find_element({xpath: "//table[@#{attr}=\"#{val}\"]"})
  end

  #alertで表示されるダイアログが表示されるかどうか
  def alert_exist?
    existence = true
    begin
      @driver.switch_to.alert
    rescue
      existence = false
    end
    existence
  end

  # myelementが表示されるようにする
  # @param myelement
  def scroll_element(myelement)
    @driver.execute_script "window.scrollTo(#{myelement.location.x},#{myelement.location.y})"
    myelement
  end
end
