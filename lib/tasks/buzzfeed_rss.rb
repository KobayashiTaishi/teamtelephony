require "open-uri"
require "rss"

class Tasks::BuzzfeedRss
  def self.execute
    data = []
    data << %w(facebook twitter 日付 url タイトル)
    facebook = ThirdPartyApi::Facebook.new
    lists = [
      'http://www.buzzfeed.com/travel.xml'
    ]
    insert_data = []

    lists.each do |rss|
      rssdata = RSS::Parser.parse(rss)

      begin
        rssdata.items.each do |entry|
          data << [
            facebook.share_count(entry.link),
            Scrapers::NokogiriScraper.twitter_share_count(entry.link),
            entry.pubDate.to_date.to_s,
            entry.link,
            entry.title
          ]
        end
      rescue => e
        # エラー時の処理
      end
    end
    CSV.open("BuzzFeed分析(travel).csv", "w") do |csv|
      data.each do |bo|
        csv << bo
      end
    end
  end
end
