# bundle exec rails runner Tasks::Ref.execute
require "open-uri"

class Tasks::Ref
  def self.execute
    # category = "keyperson"
    # category = "beauty"
    # category = "culture"
    category = "issue"
    # category = "lifestyle"

    # category = "sports"
    # category = "technology"
    # category = "travel"
    # category = "trend"

    table = CSV.table("lib/tasks/#{category}.csv")
    data = []
    data << ["url", "ref", "text"]
      # facebook = ThirdPartyApi::Facebook.new

      # begin
      table.each do |row|
        begin
          charset = nil
          html = open("#{row.field(5)}") do |f|
            charset = f.charset # 文字種別を取得
            f.read # htmlを読み込んで変数htmlに渡す
          end
          # htmlをパース(解析)してオブジェクトを作成
          doc = Nokogiri::HTML.parse(html, nil, charset)
          # binding.pry
          # jsonこんな感じになる
          # {"count"=>471, "url"=>"http://gigazine.net/news/20140925-emma-nude-hoax/"}
          # json = JSON.parse(doc.xpath('/@class="hoge"/p/text()').text)
          # JSON.parse(doc.xpath('//div/p/span/a').last)
          # doc.xpath('//div/p/span')
          data << [
            row.field(5),
            doc.xpath('//div/p/span/a').last.attributes["href"].value,
            doc.xpath('//div/p/span/a').last.children.text()
          ]
        # end
        rescue
        end
      end
      CSV.open("lib/tasks/ref-#{category}.csv", "wb") do |csv|
        data.each do |bo|
          csv << bo
        end
      end
      Rails.logger.info "complite"

    # rescue => e
    #   p e
    #   binding.pry
    #   CSV.open("lib/tasks/error-#{category}.csv","wb") do |csv|
    #     data.each do |bo|
    #       csv << bo
    #     end
    #   end
    # end
  end
end
