# bundle exec rails runner Tasks::TabilaboNewCsv.execute

class Tasks::TabilaboNewCsv
  def self.execute
    # category = "keyperson"
    # category = "beauty"
    # category = "culture"
    # category = "issue"

    category = "lifestyle"

    # category = "sports"
    # category = "technology"
    # category = "travel"
    # category = "trend"

    table = CSV.table("lib/tasks/scraping/new-#{category}.csv")
    data = []
    data << %w(SNSのPV タイトル サブタイトル url facebook_shere_count tweet_count related_tweet)
    twitter = ThirdPartyApi::Twitter.new

    begin
      table.each do |row|
        data << [
          row.field(0),
          row.field(1),
          row.field(2),
          row.field(3),
          row.field(4),
          row.field(5),
          twitter.get_tweets(row.field(1))
        ]
      end
      CSV.open("lib/tasks/scraping/tabilabo-complite-#{category}.csv", "wb") do |csv|
        data.each do |bo|
          csv << bo
        end
      end
      Rails.logger.info "complite"
    rescue => e
      Rails.logger.warn e
      CSV.open("lib/tasks/scraping/error-2-#{category}.csv", "wb") do |csv|
        data.each do |bo|
          csv << bo
        end
      end
    end
  end
end
