# bundle exec rails runner Tasks::TabilaboCsv.execute
class Tasks::TabilaboCsv
  def self.execute
    # category = "keyperson"
    # category = "beauty"
    # category = "culture"
    # category = "issue"
    category = "lifestyle"

    # category = "sports"
    # category = "technology"
    # category = "travel"
    # category = "trend"

    table = CSV.table("lib/tasks/scraping/tabi-labo-#{category}.csv")
    data = []
    data << %w(SNSのPV タイトル サブタイトル url facebook_shere_count tweet_count)
    facebook = ThirdPartyApi::Facebook.new

    begin
      table.each do |row|
        data << [
          row.field(0),
          row.field(1),
          row.field(2),
          row.field(3),
          facebook.share_count(row.field(3)),
          Scrapers::NokogiriScraper.twitter_share_count(row.field(3))
        ]
      end
      CSV.open("lib/tasks/scraping/new-#{category}.csv", "wb") do |csv|
        data.each do |bo|
          csv << bo
        end
      end
      Rails.logger.info "complite"
    rescue => e
      Rails.logger.warn e

      CSV.open("lib/tasks/scraping/error-#{category}.csv", "wb") do |csv|
        data.each do |bo|
          csv << bo
        end
      end
    end
  end
end
