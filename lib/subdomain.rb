class Subdomain
  def self.matches?(request)
    # wwwの場合は追加するルーティングにマッチングしないようにしておく
    request.subdomain.present? && request.subdomain != 'www' && request.subdomain != "8aeb950"
  end
end
