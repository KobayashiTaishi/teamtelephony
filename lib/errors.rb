module Errors
  class CannotCreateTagsError < RuntimeError; end
  class NotFoundError < RuntimeError; end
end
