# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'qolony'
set :repo_url, 'git@github.com:Campus-Inc/qolony.git'

set :deploy_to, '/var/www/rails-production-ec2-app'
set :scm, :git
set :log_level, :debug
set :pty, true
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets public/assets}
# set :default_env, { path: "/usr/local/rbenv/shims:/usr/local/rbenv/bin:$PATH" }
set :keep_releases, 5
set :rbenv_type, :user
set :rbenv_ruby, '2.2.1'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value
# nokogiriをシステムのライブラリを利用しつつbundle installするコマンド
set :bundle_env_variables, { nokogiri_use_system_libraries: 1 }

after 'deploy:publishing', 'deploy:restart'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  # desc 'Restart application'
  # task :restart do
  # end
  SSHKit.config.command_map[:rake] = 'bundle exec rake'

  desc 'Restart application'
  task :restart do
    invoke 'unicorn:restart'
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
      # execute "cd #{release_path} && /home/ec2-user/.rbenv/versions/2.2.1/bin/bundle exec rails runner script/scraping_server restart -e production"
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  desc 'Run asset_sync'
  task :asset_sync do
    run_locally do
      with rails_env: fetch(:rails_env) do
        execute :rake, 'assets:precompile'
      end
    end
  end

  desc 'Upload manifest'
  task :upload_manifest do
    on roles(:app) do
      if test "[ ! -d #{release_path}/public/assets ]"
        execute "mkdir -p #{release_path}/public/assets"
      end
      file_path = Dir.glob('public/assets/manifest*').first
      execute "rm #{release_path}/#{file_path}"
      upload!(file_path, "#{release_path}/public/assets/")
    end
  end

  # after :deploy, :asset_sync
  # after :asset_sync, :upload_manifest
  after :publishing, :restart
end
