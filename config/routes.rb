require 'sidekiq/web'
require Rails.root.join('lib', 'subdomain.rb')

Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/rails_admin', as: 'rails_admin'
  mount Sidekiq::Web => '/sidekiq'
  get "debug" => "sessions#debug", :as => "debug"
  namespace :admin do
    resources :users, only: [:index, :new, :create]
  end

  constraints Subdomain do # サブドメインが指定されていた場合に以下のルーティングが利用される
    namespace :settings do
      root to: "base#index"
    end
  end
  root to:  'client#launch'

  # Todo ローンチする前にurlをかえる

  # for api
  use_doorkeeper
  namespace :api, defaults: {format: 'json'} do
    # private api
    scope 'p', module: 'private' do
      resources :users, only: [:index]
      resources :articles, except: [:destroy, :edit] do
        collection do
          get 'tags'
          post 'upload_image'
          post 'upload_image_url'
          post 'upload_crop_image'
        end
      end
      post "/log/:event" => "log#event"
    end

    # public api
    scope ':versions', module: 'versions' do
      resources :users, except: [:new, :edit, :update]
    end
  end

  scope 'twilio', module: 'twilio' do
    resources :task_router, only: [] do
      collection do
        get :agent
        post :workflow
        post :assignment
      end
    end
  end
  resources :twilio, only: [] do
    collection do
      post :incoming
      post :outgoing
      post :collect_digit
      post :queue
      post :enqueue
      post :wait_enqueue
    end
  end
  # get '/:id', to: 'articles#show', constraints: { id: /\d+/ }, as: 'article'
  # get "hashtag/:name" => "articles#tag"

  resources :client_test
  # create newはadminで作る
  resources :users, only: [:index, :show]
  # resources :users, only: [:index, :show, :create, :new]
  resources :sessions, only: [:new, :create, :destroy] do
    collection do
      get :demo_first_user
      get :demo_second_user
      get :demo_third_user
    end
  end
  resources :articles, only: [:index, :show]
  get "logout" => "sessions#destroy", :as => "logout"
  get "login" => "sessions#new", :as => "login"
  get "twitter_login" => "sessions#twitter_login"
  get '/auth/:provider/callback', :to => 'sessions#callback'
  post '/auth/:provider/callback', :to => 'sessions#callback'
  get "/signup" => "sessions#signup"
  post '/sessions', :to => 'sessions#callback'
  resources :sessions do
    collection do
      post 'create_acount'
    end
  end
end
