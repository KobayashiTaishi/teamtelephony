require 'aws-sdk'

Aws.config[:region] = 'ap-northeast-1'
Aws.config[:credentials] = Aws::Credentials.new(Rails.application.secrets.aws_access_key, Rails.application.secrets.aws_secret_key)
