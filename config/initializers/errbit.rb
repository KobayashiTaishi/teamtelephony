
# Require the airbrake gem in your App.
# ---------------------------------------------
#
# Rails 3 - In your Gemfile
# gem 'airbrake'
#
# Rails 2 - In environment.rb
# config.gem 'airbrake'
#
# Then add the following to config/initializers/errbit.rb
# -------------------------------------------------------

if Rails.env.production?
  Airbrake.configure do |config|
    config.api_key = 'af52648b6dbdcf035776da12ea2e1fbd'
    config.host    = 'tanu-error.herokuapp.com'
    config.port    = 80
    config.secure  = config.port == 443
  end
end
