yml = YAML.load_file(Rails.root.to_path + "/config/cloudinary.yml")
Cloudinary.config do |config|
  config.cloud_name = yml[Rails.env]["cloud_name"]
  config.api_key = yml[Rails.env]["api_key"]
  config.api_secret =  yml[Rails.env]["api_secret"]
  config.cdn_subdomain = false
end
