## プロダクト画面

右側に電話があって、チームに対して発信できる
ステータスはリアルタイムに適用される

[![https://gyazo.com/74411be9043fc197fc9f6b911924ad45](https://i.gyazo.com/74411be9043fc197fc9f6b911924ad45.png)](https://gyazo.com/74411be9043fc197fc9f6b911924ad45)

ログが残るようになっている

[![https://gyazo.com/b38126c1edaa7c80dc098417e93c73a5](https://i.gyazo.com/b38126c1edaa7c80dc098417e93c73a5.png)](https://gyazo.com/b38126c1edaa7c80dc098417e93c73a5)

通話のが文字化されテキストで見れる(未実装：アドバンスドメディアのAPIを利用予定)

[![https://gyazo.com/9e5b87ed4b7176212663b81e48bd9341](https://i.gyazo.com/9e5b87ed4b7176212663b81e48bd9341.png)](https://gyazo.com/9e5b87ed4b7176212663b81e48bd9341)


## 使っている技術

- ruby on rails(サーバーサイドアプリケーションフレームワーク)
- firebase (リアルタイム通信サーバーかつDB)
- twilio (電話のAPI)
- redux / react (フロントエンドアプリケーションフレームワーク)

=========

develop環境ではrake db:seedでテストデータを入れる。
テストアカウントはdb/seeds.rbを参照

## test

```
be guard // rails
npm run test:watch // js
```

## build

```
bundle exec rails s // rails
npm run build:watch // js
```

## フロントエンド開発

htmlはコンポーネント化して使いまわせるように
containerでcomponentを組み合わせてアプリを作っていく

## css

scssで書く
bourbonとneatを利用する
http://bourbon.io/

componentとlayoutを分ける
componentに色や形など設定して、containerで配置や大きさを決定する。

## firebase スキーム

```
{
  logs: {
    uid1: {id: "uid1", type: "", createdAt: Firebase.ServerValue.TIMESTAMP},
    uid2: {type: ""},
    uid3: {type: ""}
  }
  messages: {
    groupUid1: {
      uid: {logId: "logUid1", createdAt: Firebase.ServerValue.TIMESTAMP},
      uid: {logId: "logUid2", createdAt: Firebase.ServerValue.TIMESTAMP},
      uid: {logId: "logUid3", createdAt: Firebase.ServerValue.TIMESTAMP},
      uid: {logId: "logUid4", createdAt: Firebase.ServerValue.TIMESTAMP}
    },
    groupUid2: {...},
    groupUid3: {...}
  },
  groups {
    uid1: {
      id: "uid1"
      name: "gyazo",
      members: {
      	taishi: true,
      	ozawa: true
      }
    },
    uid2: {
      id: "uid2"
      name: "campus",
      members: {
      	taishi: true
      }
    }
  },
  customers: {
    uid1: {
      id: "uid1",
      name: "taishi",
      groups: {
        group_uid1: {id: group_uid1},
        group_uid2: {id: group_uid2}
      }
    },
    uid2: {
      id: "uid2",
      name: "kyumon",
      groups: {
        group_uid1: true,
        group_uid2: true,
      }
    }
  },
  customers: {"+987654321", "+123456789"}
  users: {
    uuid: {uuid: id, status: 1000},  
    uuid: {uuid: id, status: 999},
    uuid: {uuid: id, status: 0},
    uuid: {},
    uuid: {}
  }
}

1000=オンライン
999=離席
0=オフライン

```

## css スプライト

```
npm install -g gulp
sudo npm install gulp.spritesmith
```
