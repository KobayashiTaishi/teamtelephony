const keymirror = require('keymirror')

export default keymirror({
  SET_WORKER_CLIENT: null,
  SET_ACTIVITY_NAME: null,
  SET_PHONE_RINGING: null,
  SET_INPUT_NUMBER: null,
  INCOMING_CALL: null,
  OUTGOING_CALL: null,
  CONNECT_CALL: null,
  DISCONNECT_CALL: null,
  STANDARD_TO_GLOBAL: null,
  STANDARD_TO_JA: null
})
