import keymirror from 'keymirror'

export const CurrentTabConstants = keymirror({
  CURRENT_TAB_UPDATE: null
})
