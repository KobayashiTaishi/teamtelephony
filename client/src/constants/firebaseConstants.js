const keymirror = require('keymirror')

export default keymirror({
  FIREBASE_REBASE_SET: null,
  FIREBASE_REF_SET: null
})
