import keymirror from 'keymirror'

export const CurrentUserConstants = keymirror({
  CURRENT_USER_GET: null,
  CURRENT_USER_SET: null
})
