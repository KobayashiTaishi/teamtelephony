import keymirror from 'keymirror'

export const VideoConstants = keymirror({
  VIDEO_PLAY: null,
  VIDEO_SET: null
})
