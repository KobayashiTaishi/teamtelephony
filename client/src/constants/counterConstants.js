const keymirror = require('keymirror')

export default keymirror({
  INCREMENT_COUNTER: null,
  DECREMENT_COUNTER: null
})
