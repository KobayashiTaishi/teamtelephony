import { FIREBASE_REF_SET } from '../constants/firebaseConstants'
const initialState = null;

// firebase 書き込み
export default function firebaseRef(state = initialState, action) {
  switch (action.type) {
  case FIREBASE_REF_SET:
    return action.value
  default:
    return state;
  }
}
