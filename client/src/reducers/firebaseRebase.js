import Rebase from 're-base'
import { FIREBASE_REBASE_SET } from '../constants/firebaseConstants'
const initialState = null;

// firebase 読み込み
// re-baseというライブラリを使う
export default function firebaseRebase(state = initialState, action) {
  switch (action.type) {
  case FIREBASE_REBASE_SET:
    return action.value
  default:
    return state;
  }
}
