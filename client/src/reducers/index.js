import { combineReducers } from 'redux'
import users from './users'
import currentUser from './currentUser'
import setting from './setting'
import phone from './phone'
import firebaseRef from './firebaseRef'
import firebaseRebase from './firebaseRebase'

const rootReducer = combineReducers({
  setting,
  phone,
  firebaseRef,
  firebaseRebase,
  currentUser,
  users
})

export default rootReducer
