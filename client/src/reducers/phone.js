import { Map } from 'immutable'
import initialData from '../initialData'
import { INCOMING_CALL, OUTGOING_CALL, CONNECT_CALL, DISCONNECT_CALL, STANDARD_TO_GLOBAL, STANDARD_TO_JA, SET_WORKER_CLIENT, SET_ACTIVITY_NAME, SET_PHONE_RINGING, SET_INPUT_NUMBER } from '../constants/callActionTypes'

// state
// "incoming"
// "idle"
// "outgoing" keypad
// "talking"
// const initialState = initialData.get().setting

// telephoneNumberStandard
// "E164"
// "JA"
let initialState
if (initialData.isInitialized()) {
  initialState = {
    workerClient: undefined,
    activityName: undefined,
    phoneNumbers: initialData.get().team.phoneNumbers,
    inputNumber: "",
    ringing: false,
    telephoneNumberStandard: "JA",
    status: "idle"
  }
} else {
  initialState = {
    workerClient: undefined,
    activityName: undefined,
    phoneNumbers: [],
    inputNumber: "",
    ringing: false,
    telephoneNumberStandard: "JA",
    status: "idle"
  }
}

export default function phone(state = initialState, action) {
  switch (action.type) {
  case SET_WORKER_CLIENT:
    return Map(state).merge({workerClient: action.workerClient}).toObject()
  case SET_ACTIVITY_NAME:
    return Map(state).merge({activityName: action.activityName}).toObject()
  case SET_INPUT_NUMBER:
    return Map(state).merge({inputNumber: action.inputNumber}).toObject()
  case SET_PHONE_RINGING:
    return Map(state).merge({ringing: action.ringing}).toObject()
  case STANDARD_TO_GLOBAL:
    return Map(state).merge({telephoneNumberStandard: "E164"}).toObject()
  case STANDARD_TO_JA:
    return Map(state).merge({telephoneNumberStandard: "JA"}).toObject()
  case INCOMING_CALL:
    return Map(state).merge({status: "incoming"}).toObject()
  case OUTGOING_CALL:
    return Map(state).merge({status: "outgoing"}).toObject()
  case CONNECT_CALL:
    return Map(state).merge({status: "talking"}).toObject()
  case DISCONNECT_CALL:
    return Map(state).merge({status: "idle"}).toObject()
  default:
    return state
  }
}
