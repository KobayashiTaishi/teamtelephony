// import { ADD_TODO, DELETE_TODO, EDIT_TODO, COMPLETE_TODO, COMPLETE_ALL, CLEAR_COMPLETED } from '../constants/ActionTypes';

import initialData from '../initialData'
import { Record, List } from 'immutable'

export const User = Record({
  uid: undefined,
  name: undefined,
  image: undefined,
  email: undefined,
  online: false,
})

let initialState
if (initialData.isInitialized()) {
  let userRecords = initialData.get().users.map(user => new User({
              uid: user.uid,
              name: user.name,
              image: user.image,
              email: user.email
            }))
  initialState = List(userRecords)
} else {
  initialState = List([new User({uid: "idididi", name: "taishi", image: "http://res.cloudinary.com/dkqf51oki/image/upload/l5lngufstt52sxewcbpa.jpg", email: "kbystis@gmail.com"})])
}

export default function users(state = initialState, action) {
  switch (action.type) {
  // case ADD_TODO:
  //   return [{
  //     id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1,
  //     completed: false,
  //     text: action.text
  //   }, ...state];
  //
  // case DELETE_TODO:
  //   return state.filter(todo =>
  //     todo.id !== action.id
  //   );
  //
  // case EDIT_TODO:
  //   return state.map(todo =>
  //     todo.id === action.id ?
  //       Object.assign({}, todo, { text: action.text }) :
  //       todo
  //   );
  //
  // case COMPLETE_TODO:
  //   return state.map(todo =>
  //     todo.id === action.id ?
  //       Object.assign({}, todo, { completed: !todo.completed }) :
  //       todo
  //   );
  //
  // case COMPLETE_ALL:
  //   const areAllMarked = state.every(todo => todo.completed);
  //   return state.map(todo => Object.assign({}, todo, {
  //     completed: !areAllMarked
  //   }));
  //
  // case CLEAR_COMPLETED:
  //   return state.filter(todo => todo.completed === false);

  default:
    return state;
  }
}
