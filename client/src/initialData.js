import humps from 'humps'

class initialData {
  constructor() {
    if  (global.gon) {
      let dataFromServer = humps.camelizeKeys(global.gon)
      this.data = dataFromServer
      this.initialized = true
    } else {
      this.data = null
      this.initialized = false
    }
  }
  get() {
    return this.data
  }
  isInitialized() {
    return this.initialized
  }
}

export default new initialData()
