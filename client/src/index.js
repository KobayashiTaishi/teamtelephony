import React from 'react'
import ReactDom from 'react-dom'
import initialData from './initialData'
import Root from './containers/Root'
import { Provider } from 'react-redux'
import configureStore from './store'

navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia)
if (navigator.getUserMedia) {
  navigator.getUserMedia (
    {video: false, audio: true},
    (localMediaStream) => { console.log(localMediaStream) },
    (err) => { console.log("The following error occured: " + err) }
  )
} else {
  console.log("getUserMedia not supported")
}

ReactDom.render(
  <Provider store={configureStore()}>
    <Root/>
  </Provider>, document.getElementById("root")
)
