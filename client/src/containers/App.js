import React, { Component, PropTypes } from 'react'
import Layout from './Layout'

export default class App extends Component {
  static path = '/'
  render() {
    return (
      <Layout>
        { this.props.children }
      </Layout>
    )
  }
}
