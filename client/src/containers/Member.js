import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Immutable, { Record } from 'immutable'
import ImmutablePropTypes from 'react-immutable-proptypes'
import CardContainer from '../components/cards/CardContainer'

let NetworkStatus = {}
NetworkStatus[1000] = 'オンライン'
NetworkStatus[0] = 'オフライン'
NetworkStatus[999] = '離席中'

@connect(state => ({
  firebaseUrl: state.setting.firebaseUrl,
  firebaseRebase: state.firebaseRebase,
  users: state.users,
  currentUser: state.currentUser
}))
export default class Member extends Component {
  static title = 'メンバー'
  static path = '/member'
  static propTypes = {
    firebaseUrl: PropTypes.string.isRequired,
    users: ImmutablePropTypes.listOf(Record).isRequired
  }

  constructor(props){
    super(props)
    this.state = {
      firebaseUsers: []
    }
  }
  componentWillMount() {
    this.base = this.props.firebaseRebase
    this.ref = this.base.bindToState('users', {
      context: this,
      state: 'firebaseUsers',
      asArray: true
    })
  }
  componentWillUnmount() {
    this.base.removeBinding(this.ref);
  }
  call(uid) {
    Twilio.Device.connect({'PhoneNumber': uid})
  }
  render () {
    const {
      currentUser,
      users
    } = this.props
    const { firebaseUsers } = this.state

    return (
      <CardContainer>
        <div className='member_component member_layout'>
          <h1>チームメンバー</h1>
          {(() => {
            if (users.size > 0 && firebaseUsers.length > 0) {
              return Immutable.List(firebaseUsers).reverse().map( firebaseUser => {
                let user = users.filter(u => u.uid == firebaseUser.uuid).get(0)
                if(!user) return null
                return <div key={user.uid} className='card clearfix'>
                  <img className='thumbnail' src={user.image}></img>
                  <div className='email'>{firebaseUser.status !== undefined ? NetworkStatus[firebaseUser.status] : 'オフライン'}</div>
                  <div className='name'>{user.name}</div>
                  <div className='email'>{user.email}</div>
                  { NetworkStatus[firebaseUser.status] == "オンライン" && currentUser.uid !== user.uid ? <button className='email' onClick={this.call.bind(this, user.uid)}>発信</button> : null }
                </div>
              })
            }
          })()}
        </div>
      </CardContainer>
    )
  }
}
