import React, { Component } from 'react'
import { ReactScriptLoaderMixin } from 'react-script-loader'
import reactMixin from 'react-mixin'

export default class TwilioLoader extends Component {

  constructor (props) {
    super(props)
    this.state = {
      scriptDidError: false,
      loaded: false
    }
  }
  getScriptURL() {
    return 'https://static.twilio.com/libs/twiliojs/1.2/twilio.min.js'
  }
  onScriptLoaded() {
    // twilio使えることを通知
    console.log('ロード完了')
  }
  onScriptError() {
    // エラーを出す
    this.setState({ scriptDidError: true })
    console.log('エラー')
  }
  render() {
    return null
  }
}

reactMixin(TwilioLoader.prototype, ReactScriptLoaderMixin)
