import React, { Component } from 'react'
import { ReactScriptLoaderMixin } from 'react-script-loader'
import reactMixin from 'react-mixin'
import TwilioLoader from './TwilioLoader'
import TwilioWorkerLoader from './TwilioWorkerLoader'

export default class Loader extends Component {
  render() {
    return (
      <div className="loader_component loader_layout max_height">
        <div className="circle-loader"/>
        <div className="loading_title">
          Loading...
        </div>
      </div>
    )
  }
}
