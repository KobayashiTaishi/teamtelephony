import React, { Component } from 'react'
import { ReactScriptLoaderMixin } from 'react-script-loader'
import reactMixin from 'react-mixin'

export default class TwilioWorkerLoader extends Component {

  constructor (props) {
    super(props)
    this.state = {
      scriptDidError: false,
      loaded: false
    }
  }
  getScriptURL() {
    return 'https://media.twiliocdn.com/taskrouter/js/v1.1.1/taskrouter.min.js'
  }
  onScriptLoaded() {
    // twilio使えることを通知
    console.log('ロード完了')
  }
  onScriptError() {
    // エラーを出す
    this.setState({ scriptDidError: true })
    console.log('エラー')
  }
  render() {
    return null
  }
}

reactMixin(TwilioWorkerLoader.prototype, ReactScriptLoaderMixin)
