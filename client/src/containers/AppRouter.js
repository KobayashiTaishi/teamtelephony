import React, { Component, PropTypes } from 'react'
import { Router, Route, Link, IndexRoute } from 'react-router'
import App from '../containers/App'
import Member from '../containers/Member'
import Board from '../containers/Board'
import Messages from '../containers/Messages'
import Activity from '../components/Activity'

export default class AppRouter extends Component {
  render() {
    return (
      <Router>
        <Route path={App.path} component={App}>
          <IndexRoute component={Messages} >
            <Route path={Board.path} component={Board} />
          </IndexRoute>
          <Route path={Member.path} component={Member} />
          <Route path={Activity.path} component={Activity} />
          <Route path={Messages.path} component={Messages}>
            <Route path={Board.path} component={Board} />
          </Route>
        </Route>
      </Router>
    )
  }
}
