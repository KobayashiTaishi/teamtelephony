import React, { Component, PropTypes } from 'react'
import Header from '../components/Header'
import NavigationBar from '../components/NavigationBar'
import Phone from './twilio/Phone'
import Worker from './Worker'

export default class Layout extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element)
    ]).isRequired
  }

  render () {
    return (
      <div className='layout_container max_height'>
        <Header fixed={false}/>
        <NavigationBar/>
        <Worker/>
        <div className='children_component max_height'>
          {
            this.props.children
          }
          <Phone/>
        </div>
      </div>
    )
  }
}
