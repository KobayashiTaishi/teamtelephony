import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Immutable, { Record } from 'immutable'
import CardContainer from '../components/cards/CardContainer'
import { Link } from 'react-router'

@connect(state => ({
  firebaseUrl: state.setting.firebaseUrl,
  firebaseRebase: state.firebaseRebase,
  users: state.users,
  currentUser: state.currentUser
}))
export default class Messages extends Component {
  static title = 'メッセージ'

  static propTypes = {
  }

  constructor(props){
    super(props)
    this.state = {
      boards: []
    }
  }

  componentWillMount() {
    this.base = this.props.firebaseRebase
    this.ref = this.base.bindToState('boards', {
      context: this,
      state: 'boards',
      asArray: true
    })
  }

  componentWillUnmount() {
    this.base.removeBinding(this.ref);
  }

  render () {
    const { boards } = this.state

    return (
      <CardContainer>
        <div className='messages_container'>
          <div className='message_list clearfix'>
            {(() => {
              return Immutable.List(boards).reverse().map((board, index) => {
                return (
                  <div key={index} className='card'>
                    <img className="thumbnail" src="http://res.cloudinary.com/dkqf51oki/image/upload/v1446866006/sano_iiwh8t.jpg"/>
                    <span className='card_title'>
                      <Link to={'/messages/' + board.id}>
                        from: {board.customer.customerId}
                      </Link>
                    </span>
                  </div>
                )
              })
            })()}
          </div>
          <div className="board">
            {this.props.children || <div>デフォルト画面</div>}
          </div>
        </div>
      </CardContainer>
    )
  }
}
