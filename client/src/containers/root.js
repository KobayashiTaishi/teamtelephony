import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import ImmutablePropTypes from 'react-immutable-proptypes'
import AppRouter from './AppRouter'
import { setFirebaseRef, setFirebaseRebase, registerOnlineStatus } from '../actions/firebaseActions'
import { findOrCreate } from '../actions/boardActions'

@connect(state => ({
  firebaseUrl: state.setting.firebaseUrl,
  currentUser: state.currentUser
}))
export default class Root extends Component {
  static propTypes = {
    firebaseUrl: PropTypes.string.isRequired,
  }

  componentWillMount() {
    const { firebaseUrl, dispatch } = this.props
    dispatch(setFirebaseRef(firebaseUrl))
    dispatch(registerOnlineStatus())
    dispatch(setFirebaseRebase(firebaseUrl, 'taishi_team'))
    let customerNumber = "+811232001734"
    let myNumber = "+8111111144"
    dispatch(findOrCreate(myNumber, customerNumber))
  }

  render() {
    return <AppRouter/>
  }
}
