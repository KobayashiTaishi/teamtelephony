import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Immutable, { Record } from 'immutable'
import { Link } from 'react-router'

@connect(state => ({
  firebaseUrl: state.setting.firebaseUrl,
  firebaseRebase: state.firebaseRebase,
  users: state.users,
  currentUser: state.currentUser
}))
export default class Boards extends Component {
  static propTypes = {
  }

  constructor(props){
    super(props)
    this.state = {
      boards: []
    }
  }

  componentWillMount() {
    this.base = this.props.firebaseRebase
    this.ref = this.base.bindToState('boards', {
      context: this,
      state: 'boards',
      asArray: true
    })
  }

  componentWillUnmount() {
    this.base.removeBinding(this.ref);
  }

  render () {
    const { boards } = this.state

    return (
      <div>
        {(() => {
          return Immutable.List(boards).reverse().map((board, index) => {
            return (
              <div key={index} className='card clearfix'>
                <span className='id'>id:</span>
                <div className='name'>
                  <Link to={'/boards/' + board.id}>
                    {board.id}
                  </Link>
                </div>
              </div>
            )
          })
        })()}
      </div>
    )
  }
}
