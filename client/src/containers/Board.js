import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Immutable, { Record, Seq } from 'immutable'

@connect(state => ({
  firebaseUrl: state.setting.firebaseUrl,
  firebaseRebase: state.firebaseRebase,
  currentUser: state.currentUser
}))
export default class Board extends Component {
  static path = '/messages/:boardId'
  static propTypes = {
  }

  constructor(props){
    super(props)
    this.state = {
      board: null
    }
  }

  componentWillMount() {
    this.base = this.props.firebaseRebase
    this._bind()
  }

  componentWillReceiveProps(nextProps) {
    this.base.removeBinding(this.ref)
    this._bind()
  }

  _bind() {
    this.ref = this.base.bindToState('/boards/' + this.props.params.boardId, {
      context: this,
      state: 'board',
      asArray: false
    })
  }

  componentWillUnmount() {
    this.base.removeBinding(this.ref)
  }

  render () {
    const { board } = this.state
    return (
      <div>
        {(() => {
          if(board) {
            return (
              <div className="board_container">
                <div className='id'>customerId: {board.customer.customerId}</div>
                {(() => {
                  return Seq(board.members).map((value, key) =>
                    <div key={key} className='card clearfix'>
                      <div className='number'>{value.number}</div>
                    </div>
                  )
                })()}
              </div>
            )
          }
        })()}
      </div>
    )
  }
}
