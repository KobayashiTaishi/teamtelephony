import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReactDom from 'react-dom'
const TwilioBase = require("./TwilioBase")
const IncomingPhone = require("../../components/IncomingPhone")
const Promise = require("bluebird")
import { ringing, notRinging, input } from '../../actions/callActions'

@connect(state => ({
  currentUser: state.currentUser,
  phone: state.phone,
  telephoneNumberStandard: state.phone.telephoneNumberStandard,
  inputNumber: state.phone.inputNumber,
  phoneNumbers: state.phone.phoneNumbers,
  ringing: state.phone.ringing,
  capabilityToken: state.currentUser.capabilityToken
}))
class Phone extends TwilioBase {
  constructor (props) {
    super(props)
  }
  // @override
  incomingHandler() {
    Twilio.Device.incoming(connection => {
      const { connect, dispatch } = this.props
      console.log(connection.parameters)
      this.deferred = Promise.defer()
      this.deferred.promise.then(obj=>{
        connection.accept()
        dispatch(notRinging())
      })
      .catch(error=>{
        connection.reject()
        dispatch(notRinging())
      })
      dispatch(ringing())
    })
  }
  // @override
  connect() {
    Twilio.Device.connect({PhoneNumber: "+81" + this.props.inputNumber})
  }
  pressKey(key) {
    const { inputNumber, dispatch } = this.props
    dispatch(input(inputNumber + key))
    ReactDom.findDOMNode(this.refs.phoneInput).focus()
  }
  handleChange(e) {
    const { dispatch } = this.props
    dispatch(input(e.target.value))
  }
  render() {
    const { inputNumber, ringing, telephoneNumberStandard, phoneNumbers } = this.props
    return (
      <div className="phone_component phone_layout">
        {(() => {
          if(ringing) {
            return <IncomingPhone promise={this.deferred}/>
          } else {
            return (<div>
              <div>国内or国際</div>
              <div>{phoneNumbers[0]}</div>
              <div className="dialed_number_wrapper">
                <div className="country_select">+81</div>
                <input type="text" className="dialed_number" ref="phoneInput" value={inputNumber} onChange={this.handleChange.bind(this)} placeholder="名前、電話番号"/>
                <div className="backspace_wrapper">
                  <div className="backspace_button">戻る</div>
                </div>
              </div>
              <ul className="phone_number_buttons">
                <li onClick={this.pressKey.bind(this, "1")} className="phone_number_button">1</li>
                <li onClick={this.pressKey.bind(this, "2")} className="phone_number_button">2</li>
                <li onClick={this.pressKey.bind(this, "3")} className="phone_number_button">3</li>
                <li onClick={this.pressKey.bind(this, "4")} className="phone_number_button">4</li>
                <li onClick={this.pressKey.bind(this, "5")} className="phone_number_button">5</li>
                <li onClick={this.pressKey.bind(this, "6")} className="phone_number_button">6</li>
                <li onClick={this.pressKey.bind(this, "7")} className="phone_number_button">7</li>
                <li onClick={this.pressKey.bind(this, "8")} className="phone_number_button">8</li>
                <li onClick={this.pressKey.bind(this, "9")} className="phone_number_button">9</li>
                <li onClick={this.pressKey.bind(this, "*")} className="phone_number_button">*</li>
                <li onClick={this.pressKey.bind(this, "0")} className="phone_number_button">0</li>
                <li onClick={this.pressKey.bind(this, "#")} className="phone_number_button">#</li>
              </ul>
              <button onClick={::this.connect} className="outbound_button">発信</button>
            </div>)
          }

        })()}

      </div>
    )
  }
}

module.exports = Phone
