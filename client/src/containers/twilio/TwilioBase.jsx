import React, { Component } from 'react'
import { ReactScriptLoaderMixin } from 'react-script-loader'
const reactMixin = require("react-mixin")

class TwilioBase extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      scriptDidError: false,
      loaded: false
    }
  }
  getScriptURL() {
    return "https://static.twilio.com/libs/twiliojs/1.2/twilio.min.js"
  }
  onScriptLoaded() {
    // ブロック
    if(this.state.loaded) return

    this.setState({ loaded: true })
    Twilio.Device.setup(this.props.capabilityToken, {debug: true})

    this.readyHandler()
    this.connectHandler()
    this.errorHandler()
    this.disconnectHandler()
    this.incomingHandler()
    this.presenceHandler()
  }
  readyHandler() {
    Twilio.Device.ready(device => {
      console.log("Ready")
      console.log(device)
    })
  }
  errorHandler() {
    Twilio.Device.error(error => {
      if(error.code == 31205) {
        // get capabilityToken because token is expired.
      }
      console.log("Error" + error.message)
    })
  }
  connectHandler() {
    Twilio.Device.connect(conn => {
      console.log("Successfully established call")
      console.log("conn = ")
      console.log(conn)
    })
  }
  disconnectHandler() {
    Twilio.Device.disconnect(()=>{
    })
  }
  incomingHandler() {
    Twilio.Device.incoming(connection => {
      console.log("Incoming connection from " + connection.parameters.From)
      connection.accept()
    })
  }
  presenceHandler() {
    Twilio.Device.presence(pres => {
      // ステータスが変わった
    })
  }
  connect() {
    Twilio.Device.connect()
  }
  hangup() {
    Twilio.Device.disconnectAll()
  }
  onScriptError() {
    this.setState({ scriptDidError: true })
  }
  render() {
    return (
      <div>継承</div>
    )
  }
}

reactMixin(TwilioBase.prototype, ReactScriptLoaderMixin)

module.exports = TwilioBase
