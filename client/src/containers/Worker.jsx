import React, { Component } from 'react'
import { connect } from 'react-redux'
import { incoming, setWorkerClient, changeActivity } from '../actions/callActions'

@connect(state => ({
  currentUser: state.currentUser,
  phone: state.phone
}))
export default class Worker extends Component {
  constructor (props) {
    super(props)
    this.activitySids = {}
    this.called = false
  }
  componentWillMount() {
    const { currentUser, dispatch } = this.props
    const workerClient = new global.Twilio.TaskRouter.Worker(currentUser.workerToken)
    dispatch(setWorkerClient(workerClient))
  }

  fetchActivitySids() {
    const { phone } = this.props
    phone.workerClient.activities.fetch((error, activityList) => {
      let activities = activityList.data
      activities.forEach((data, index) => {
        this.activitySids[activities[index].friendlyName] = activities[index].sid
      }.bind(this))
    })
  }
  registerTaskRouterCallbacks() {
    const { phone, dispatch } = this.props
    phone.workerClient.on('ready', worker => {
      dispatch(changeActivity(worker.activityName))
      console.log('Successfully registered as: ' + worker.friendlyName)
    }.bind(this))

    phone.workerClient.on('activity.update', worker => {
      dispatch(changeActivity(worker.activityName))
    }.bind(this))

    phone.workerClient.on('reservation.created', reservation => {
      console.log('You have been reserved to handle a call!')
      console.log('Call from: ' + reservation.task.attributes.from)
      const { dispatch } = this.props
      dispatch(incoming(reservation))
      // fetch_or_create_message(massages)
    }.bind(this))

    phone.workerClient.on('reservation.accepted', reservation => {
      console.log('Reservation ' + reservation.sid + ' accepted!')
    }.bind(this))

    phone.workerClient.on('reservation.rejected', reservation => {
      // TODO: 拒否の
      console.log('Reservation ' + reservation.sid + ' rejected!')
    }.bind(this))

    phone.workerClient.on('reservation.timeout', reservation => {
      // TODO: タイムアウト
      console.log('Reservation ' + reservation.sid + ' timed out!')
    }.bind(this))

    phone.workerClient.on('reservation.canceled', reservation => {
      // TODO: キャンセルの
      console.log('Reservation ' + reservation.sid + ' canceled!')
    }.bind(this))
  }

  // オンラインと離席中を切り替え
  changeStatus() {
    const { phone, dispatch } = this.props
    let nextActivityName
    if(this.state.activityName == 'Idle') {
      nextActivityName = 'Busy'
    } else {
      nextActivityName = 'Idle'
    }
    phone.workerClient.updateActivity(this.activitySids[nextActivityName])
    dispatch(changeActivity(nextActivityName))
  }

  render() {
    const { phone } = this.props
    if(phone.workerClient && !this.called) {
      this.registerTaskRouterCallbacks()
      this.fetchActivitySids()
      this.called = true
    }
    return (
      <div className='worker_component worker_layout'>
        <span className='status' onClick={::this.changeStatus}>
          Status: {phone.activityName}
        </span>
      </div>
    )
  }
}
