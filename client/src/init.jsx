import React, {Component} from 'react'
import appConstants from './appConstants'
const humps = require("humps")
window.pc = window.matchMedia("(min-width: 769px)").matches
window.env = document.querySelector("html").dataset.env
const entryParams = humps.camelizeKeys(gon)
appConstants.setEnv(window.env)
appConstants.setTeamUUID(entryParams.team.uuid)
window.workerToken = entryParams.currentUser.workerToken
navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia)

if (navigator.getUserMedia) {
  navigator.getUserMedia (
    {video: false, audio: true},
    (localMediaStream) => { console.log(localMediaStream) },
    (err) => { console.log("The following error occured: " + err) }
  )
} else {
  console.log("getUserMedia not supported")
}

React.initializeTouchEvents(true)
// サーバーサイドから初期データをjsonデータを渡す(注意：globalなので命名を注意)
// using gon(rails gem)
let userStore = require("./stores/userStore")
userStore.fetch()


// firebase
// dataStore.child('messages').push({
//   'message': 'こんにちは'
// })
let Firebase = require("firebase")
let dataStore = new Firebase(appConstants.FIREBASE_URL + "/taishi_team")

dataStore.authWithCustomToken(entryParams.currentUser.firebaseToken, (error, authData) => {
  if (error) {
    console.log("Login Failed!", error)
  } else {
    console.log("Login Succeeded!", authData)
  }
})

// つなげた関数を返す
// PROBREM: Bを検索したいとき、もしBがAの部分集合の場合、検索結果にAとBの両方とも出力される
function testFunc(array) {
  return orderByChild('taishi').startAt(true).endAt(true)
}

// hoge
// piyoを引数にとって,group idを探す
// dataStore.child("users/" + id)
// dataStore.child("users/" + id)

let groups = dataStore.child("groups")
let customers = dataStore.child("customer")
let customerNumber = "+819082174090"
let myNumber = "+8111111181"
let groupMember = [customerNumber, myNumber]
// groups.push().set({members: {yamanatsu: true, taishi: true}})
customers.child(customerNumber).once("value", customer => {
  if(customer.val()) {
    // 探す
    Object.keys(customer.val().groups).forEach(group_uid => {
      groups.child(group_uid).once("value", group => {
        // そもそもこの場合はおかしい
        console.log("一度見たことあるユーザーさん")
        if (!group.val()) return

        if(equal(groupMember ,Object.keys(group.val().members))) {
          console.log("前のカルテを出しますね")
        }
      })
    })

  } else {
    // ユーザーを作る
    console.log("新しいユーザーさんだー。")
    let customerGroups = {}, groupMembers = {}
    customerGroups[myNumber] = true
    groupMembers[myNumber] = true
    groupMembers[customerNumber] = true
    customers.child(customerNumber).set({id: customerNumber, groups: customerGroups})
    let newGroupRef = groups.push()
    newGroupRef.set({id: newGroupRef.key(), members: groupMembers})
  }
})

// let logs = dataStore.child("logs")
// let messages = dataStore.child("messages")
// logs.child('log1').set({type: "called", createdAt: Firebase.ServerValue.TIMESTAMP})
// logs.child('log2').set({type: "called", createdAt: Firebase.ServerValue.TIMESTAMP})
// logs.child('log3').set({type: "called", createdAt: Firebase.ServerValue.TIMESTAMP})
// messages.child('group1').push({logId: "log1", createdAt: Firebase.ServerValue.TIMESTAMP})
// messages.child('group1').push({logId: "log2", createdAt: Firebase.ServerValue.TIMESTAMP})
// messages.child('group1').push({logId: "log3", createdAt: Firebase.ServerValue.TIMESTAMP})

// messages.child('group1').orderByChild("createdAt").once('value', function(messagesSnap) {
//   messagesSnap.forEach(log => {
//     logs.child(log.val().logId).once('value', function(logSnap) {
//       console.log(logSnap.val().createdAt)
//     })
//   })
// })

// 配列の中身が同じか検証
function equal(array1, array2) {
  return array1.sort().toString() == array2.sort().toString()
}

let currentUserStore = require("./stores/currentUserStore")
let CurrentUserActions = require("./actions/CurrentUserActions")
let currentTabStore = require("./stores/currentTabStore")
CurrentUserActions.set(entryParams.currentUser)

let Phone = require("./components/twilio/Phone")
let Worker = require("./components/twilio/Worker")
let Layout = require("./components/Layout")
let Member = require("./components/Member")
React.render(
  <Layout>
    <Member/>
    <Worker/>
    <Phone capabilityToken={currentUserStore.getState().capabilityToken} />
  </Layout>,
    document.getElementById("react_component")
)

require("./presenceStatus")
