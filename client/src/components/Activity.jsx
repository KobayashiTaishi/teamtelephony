import React, { Component } from 'react'
import { connect } from 'react-redux'
import Immutable from 'immutable'
import CardContainer from '../components/cards/CardContainer'

@connect(state => ({
  users: state.users,
  currentUser: state.currentUser,
  firebaseRebase: state.firebaseRebase,
  firebaseUrl: state.setting.firebaseUrl
}))
export default class Activity extends React.Component {

  static title = 'アクティビティ'
  static path = '/activity'

  constructor(props){
    super(props)
    this.state = {
      logs: []
    }
  }
  componentWillMount() {
    this.base = this.props.firebaseRebase
    this.ref = this.base.bindToState('logs', {
      context: this,
      state: 'logs',
      asArray: true,
      queries: {
        orderByChild: 'createdAt',
      }
    })
  }
  componentWillUnmount() {
    this.base.removeBinding(this.ref);
  }
  removeLog(id) {
    let ref = new Firebase('https://sweltering-inferno-9811.firebaseio.com/taishi_team/logs')
    ref.child(id).remove()
  }

  addLog(key) {
    const { currentUser } = this.props
    let ref = new Firebase('https://sweltering-inferno-9811.firebaseio.com/taishi_team/logs')
    let newLogRef = ref.push()
    newLogRef.set({id: newLogRef.key(), type: 'called', from: "+81345402452", to: currentUser.uid, createdAt: Firebase.ServerValue.TIMESTAMP})
  }

  formatDate(timestamp) {
    let date = new Date(timestamp)
    var h = date.getHours(), m = date.getMinutes()
    var time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
    return date.getFullYear() + '年' + date.getMonth() + '月' + date.getDate() + '日' + time
  }

  render() {
    const { users } = this.props

    let ref = new Firebase('https://sweltering-inferno-9811.firebaseio.com/taishi_team/logs')
    return (
      <div className='activity_component'>
        <div className='side'>
          <h3>Activity</h3>
          <button className='add_button' onClick={this.addLog.bind(this)}>追加</button>
          <div>filterできるようにする</div>
        </div>
        <div className='timeline'>
          {(() => {
            return Immutable.List(this.state.logs).reverse().map( (log, index) => {
              let user = users.filter(u => u.uid == log.to).get(0)
              if (user) {
                return (
                  <div key={index} className='card clearfix'>
                    <div className='upper_part'>
                      <img className='thumbnail' src={user.image}></img>
                      <span className='text_parts'>{user.name}</span>
                      { " is called from " }
                      <span>{log.from}</span>
                      <span className='date clearfix'>{ this.formatDate(log.createdAt) }</span>
                    </div>

                    <div className='lower_part'>
                      <button className='remove_button' onClick={ this.removeLog.bind(this, log.id) }>削除</button>
                    </div>
                  </div>
                )
              } else {
                return (
                  <div key={index} className='card clearfix'>
                    <div className=''>{ this.formatDate(log.createdAt) }</div>
                    <div>{log.to}</div>
                    <button className='' onClick={ this.removeLog.bind(this, log.id) }>削除</button>
                  </div>
                )
              }
            })
          })()}
        </div>
      </div>
    )
  }
}
