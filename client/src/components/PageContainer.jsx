import React, { Component } from 'react'
const ReactDraggableTab = require("react-draggable-tab")
const Tabs = ReactDraggableTab.Tabs
const Tab = ReactDraggableTab.Tab
const CurrentTabActions = require("../actions/CurrentTabActions")
const currentTabStore = require("../stores/currentTabStore")
const pageCallbackStore = require("../stores/callbacks/pageCallbackStore")
const List = require("immutable").List
const LinkTo = require("./LinkTo")

const tabsClassNames = {
  tabBar: 'myTabBar',
  tabBarAfter: 'myTabBarAfter',
  tab: 'myTab',
  tabTitle: 'myTabTitle',
  tabCloseIcon: 'tabCloseIcon',
  tabBefore: 'myTabBefore',
  tabAfter: 'myTabAfter'
}
const tabsStyles = {
  tabBar: {},
  tab: {},
  tabTitle: {},
  tabCloseIcon: {},
  tabBefore: {},
  tabAfter: {}
}
const firstTabKey = "unclosable_tab"

class PageContainer extends React.Component {
  static propTypes = {}
  static defaultProps = {}

  constructor (props) {
    super(props)
    this.state = {
      tabs: [
        (<Tab key={firstTabKey} title={'unclosable tab'} disableClose={true} >
          <div>
            <h1>This tab cannot close</h1>
          </div>
        </Tab>),
        (<Tab key={'tab1'} title={'1stTab'} >
          <div>
            <h1>This is tab1</h1>
          </div>
        </Tab>),
        (<Tab key={'tab2'} title={'2ndTab Too long Toooooooooooooooooo long'}>
          <div>
            <pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
            </pre>
            <input type="text" className="dialed_number" placeholder="名前、電話番号"/>
          </div>
        </Tab>)
      ],
      selectedTab: firstTabKey
    }
  }
  componentWillMount() {
    currentTabStore.on('change', ::this.updateCurrentTabPage)
    pageCallbackStore.on('change', ::this.openNewTab)
  }
  componentWillUnmount() {
    currentTabStore.removeListener('change', ::this.updateCurrentTabPage)
    pageCallbackStore.removeListener('change', ::this.openNewTab)
  }
  updateCurrentTabPage() {
    let newTitle = currentTabStore.getState().title
    let newElement = currentTabStore.getState().element
    if(newTitle && newElement) {
      let selectedIndex = this.state.tabs.findIndex(tab => tab.key == this.state.selectedTab)
      let newTab = (<Tab key={this.state.tabs[selectedIndex].key} title={newTitle} >{newElement}</Tab>)
      this.setState({tabs: List(this.state.tabs).set(selectedIndex, newTab).toArray()})
    }
  }
  openNewTab() {
    let newTitle = pageCallbackStore.getState().title
    let newElement = pageCallbackStore.getState().element
    if(newTitle && newElement) {
      const key = 'tab_' + Date.now()
      var element = (<div>
                <h1>Open New Empty Tab</h1>
              </div>)
      let newTabs = this.state.tabs.concat([(<Tab key={key} title='untitled' >{ element }</Tab>)])
      this.setState({tabs: newTabs, selectedTab: key})
    }
  }
  handleTabSelect(e, key, currentTabs) {
    this.setState({selectedTab: key, tabs: currentTabs})
  }
  handleTabClose(e, key, currentTabs) {
    this.setState({tabs: currentTabs})
  }
  handleTabPositionChange(e, key, currentTabs) {
    this.setState({tabs: currentTabs})
  }
  _onClick() {
    var element = (<div>
              <h1>Open New Empty Tab</h1>
            </div>)
    pageCallbackStore.openNewPage("open title", element)
  }
  handleTabAddButtonClick(e, currentTabs) {
    const key = 'tab_' + Date.now()
    var element = (<div><h1>We can change</h1></div>)
    var divv = (<div>
              <h1>New Empty Tab</h1>
              <button onClick={::this._onClick}>New Empty Tab</button>
              <LinkTo title="piyopiyo" element={element}>
                <button>button</button>
              </LinkTo>
            </div>)
    let newTab = (<Tab key={key} title='untitled' >{ divv }</Tab>)
    let newTabs = currentTabs.concat([newTab])
    this.setState({
      tabs: newTabs,
      selectedTab: key
    })
  }

  render () {
    return (
      <div className="page_container_component page_container_layout">
        <Tabs
          tabsClassNames={tabsClassNames}
          tabsStyles={tabsStyles}
          selectedTab={this.state.selectedTab ? this.state.selectedTab : 'tab2'}
          onTabSelect={::this.handleTabSelect}
          onTabClose={::this.handleTabClose}
          onTabAddButtonClick={::this.handleTabAddButtonClick}
          onTabPositionChange={::this.handleTabPositionChange}
          tabs={this.state.tabs}
          shortCutKeys={
            {
              'close': ['alt+command+w', 'alt+ctrl+w'],
              'create': ['alt+command+t', 'alt+ctrl+t'],
              'moveRight': ['alt+command+tab', 'alt+ctrl+tab'],
              'moveLeft': ['shift+alt+command+tab', 'shift+alt+ctrl+tab']
            }
          }
        />
      </div>
    )
  }
}

module.exports = PageContainer
