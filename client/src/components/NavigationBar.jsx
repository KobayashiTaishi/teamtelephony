import React, { Component } from 'react'
import { Link } from 'react-router'

export default class NavigationBar extends Component {
  render () {
    return (
      <nav className='navigation_bar_component navigation_bar_layout max_height'>
        <div className='top_icons'>
          <Link to='/'>
            <i className='home_icon'></i>
          </Link>
          <Link to='/activity'>
            <i className='activity_icon'></i>
          </Link>
          <Link to='/member'>
            <i className='member_icon'></i>
          </Link>
        </div>
        <div className='bottom_icons'>
          <i className='reporting_icon'></i>
          <i className='setting_icon'></i>
        </div>
      </nav>
    )
  }
}
