import React, { Component } from 'react'

export default class Header extends Component {
  static propTypes = {
    fixed: React.PropTypes.bool
  }

  render() {
    var style = this.props.fixed ? {"position": "fixed"} : {}
    return (
      <header className="header_component header_layout" style={style}>
        <div className="logo">
        </div>
      </header>
    )
  }
}
