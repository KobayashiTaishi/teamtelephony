import React, { Component } from 'react'
import ReactDom from 'react-dom'
const Immutable = require("immutable")

class IncomingPhone extends React.Component {

  static propTypes = {}
  static defaultProps = {connection: null}

  componentDidMount() {
    ReactDom.findDOMNode(this.refs.accept).focus()
  }
  componentWillUnmount() {
  }
  accept() {
    this.props.promise.resolve()
  }
  decline() {
    this.props.promise.reject()
  }
  render() {
    return (
      <div className="incoming_phone_component incoming_phone_layout">
        from
        to
        name
        image
        tags
        <button onClick={::this.accept} ref="accept" className="accept_button">応答</button>
        <button onClick={::this.decline} className="decline_button">拒否</button>
      </div>
    )
  }
}

module.exports = IncomingPhone
