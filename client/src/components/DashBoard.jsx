import React, { Component } from 'react'
import { connect } from 'react-redux'
import CardContainer from '../components/cards/CardContainer'
import Boards from '../containers/Boards'

@connect(state => ({
  currentUser: state.currentUser
}))
export default class DashBoard extends React.Component {
  static title = 'ダッシュボード'

  render () {
    const { currentUser } = this.props
    return (
      <CardContainer>
        <div className='dash_board_component'>
          <h1>Boards</h1>
          <div className='name'>名前：{currentUser.name}</div>
          <div className='email'>メール：{currentUser.email}</div>
        </div>
        <Boards/>
      </CardContainer>
    )
  }
}
