import React, { Component } from 'react'
const CurrentTabActions = require("../actions/CurrentTabActions")

export default class LinkTo extends React.Component {
  constructor (props) {
    super(props)
  }

  handleOnClick() {
    let newElement = this.props.element || (<h1>Changed Tab</h1>)
    let newTitle = this.props.title || "changed title"
    CurrentTabActions.linkTo(newTitle, newElement)
  }

  render () {
    return (
      <a className="link_to_component link_to_layout" onClick={this.handleOnClick.bind(this)}>
        {
          this.props.children
        }
      </a>
    )
  }
}

LinkTo.propTypes = {
  children: React.PropTypes.element,
  element: React.PropTypes.object,
  title: React.PropTypes.string
  // children: React.PropTypes.arrayOf(React.PropTypes.element)
}
LinkTo.defaultProps = {
}
