import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReactFireMixin from 'reactfire'
import reactMixin from 'react-mixin'
import Immutable from 'immutable'
import Rebase from 're-base'

export default class CardContainer extends Component {

  render() {
    return (
      <div className='card_container_component'>
        { this.props.children }
      </div>
    )
  }
}
