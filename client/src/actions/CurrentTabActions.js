var AppDispatcher = require('../AppDispatcher')
var CurrentTabConstants = require("../constants/CurrentTabConstants")

var actionAlias = {
  linkTo: CurrentTabConstants.CURRENT_TAB_UPDATE
}

var CurrentTabActions = {}
Object.keys(actionAlias).map(function (key) {
  CurrentTabActions[key] = function () {
    AppDispatcher.handleViewAction({
      actionType: actionAlias[key],
      args: arguments
    })
  }
})

module.exports = CurrentTabActions
