var AppDispatcher = require('../AppDispatcher')
var CurrentUserConstants = require("../constants/CurrentUserConstants")

var actionAlias = {
  get: CurrentUserConstants.CURRENT_USER_GET,
  set: CurrentUserConstants.CURRENT_USER_SET
}

var CurrentUserActions = {}
Object.keys(actionAlias).map(function (key) {
  CurrentUserActions[key] = function () {
    AppDispatcher.handleViewAction({
      actionType: actionAlias[key],
      args: arguments
    })
  }
})

module.exports = CurrentUserActions
