var AppDispatcher = require('../AppDispatcher')
var humps = require("humps")
var request =require("superagent-clone-for-webworker")
var ArticleConstants = require("../constants/ArticleConstants")

var actionAlias = {
  set: ArticleConstants.ARTICLE_SET
}

var ArticleActions = {
  fetch: function (path, page) {
    request.get(path)
      .query({page: page})
      .end((_err, res)=>{
        AppDispatcher.handleViewAction({
          actionType: ArticleConstants.ARTICLE_SET,
          args: [humps.camelizeKeys(res.body)]
        })
      })
  }
}
Object.keys(actionAlias).map(function (key) {
  ArticleActions[key] = function () {
    AppDispatcher.handleViewAction({
      actionType: actionAlias[key],
      args: arguments
    })
  }
})

// ArticleActions["fetch"] = function (path, page) {
//   request.get(path)
//     .query({page: page})
//     .end((_err, res)=>{
//       AppDispatcher.handleViewAction({
//         actionType: ArticleConstants.ARTICLE_SET,
//         args: [humps.camelizeKeys(res.body)]
//       })
//     })
// }
  // fetch: ArticleConstants.ARTICLE_FETCH,

module.exports = ArticleActions
