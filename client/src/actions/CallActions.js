import { INCOMING_CALL, OUTGOING_CALL, CONNECT_CALL, DISCONNECT_CALL, STANDARD_TO_GLOBAL, STANDARD_TO_JA, SET_WORKER_CLIENT, SET_ACTIVITY_NAME, SET_PHONE_RINGING, SET_INPUT_NUMBER } from '../constants/callActionTypes'
import Firebase from 'firebase'

export function input(text) {
  return {
    inputNumber: text,
    type: SET_INPUT_NUMBER
  }
}
export function ringing() {
  return {
    ringing: true,
    type: SET_PHONE_RINGING
  }
}
export function notRinging() {
  return {
    ringing: false,
    type: SET_PHONE_RINGING
  }
}
export function changeActivity(nextActivityName) {
  return {
    activityName: nextActivityName,
    type: SET_ACTIVITY_NAME
  }
}

export function setWorkerClient(workerClient) {
  return {
    workerClient: workerClient,
    type: SET_WORKER_CLIENT
  }
}

export function incoming(reservation) {
  reservation.dequeue(
    '+81345402452',
    'WA5d9ecba52d8b5846444df496231a8d7d',
    function(error, reservation) {
        if(error) {
            console.log(error.code)
            console.log(error.message)
            return
        }
        console.log('reservation dequeued')
    }
  )
  return {
    type: INCOMING_CALL
  }
}
export function outgoing() {
  return {
    type: OUTGOING_CALL
  }
}

export function accept() {
  return {
    type: CONNECT_CALL
  }
}

export function decline() {
  return {
    type: DISCONNECT_CALL
  }
}
