export function findOrCreate(myNumber, customerNumber) {
  return (dispatch, getState) => {
    const { firebaseRef } = getState();
    let boards = firebaseRef.child('taishi_team').child("boards")
    let customers = firebaseRef.child('taishi_team').child("customer")
    let boardMember = [customerNumber, myNumber]
    customers.child(customerNumber).once("value", customer => {
      if(customer.val()) {
        Object.keys(customer.val().boards).forEach((boardId, index) => {
          boards.child(boardId).once("value", board => {
            if(equal(boardMember ,Object.keys(board.val().members))) {
              console.log("前のカルテを出しますね")
            } else if(index == Object.keys(customer.val().boards).length - 1) {
              dispatch(create(myNumber, customerNumber))
            }
          })
        })
      } else {
        // 探す
        dispatch(create(myNumber, customerNumber))
      }
    })
  }
}

export function create(myNumber, customerNumber) {
  console.log("create board")
  return (dispatch, getState) => {
    const { firebaseRef } = getState();
    let boards = firebaseRef.child('taishi_team').child("boards")
    let customers = firebaseRef.child('taishi_team').child("customer")
    let newBoardRef = boards.push()
    newBoardRef.set({id: newBoardRef.key()})
    newBoardRef.update({customer: {customerId: customerNumber}})
    newBoardRef.child('members').child(myNumber).set({number: myNumber})
    newBoardRef.child('members').child(customerNumber).set({number: customerNumber})
    customers.child(customerNumber).set({id: customerNumber})
    customers.child(customerNumber).child('boards').child(newBoardRef.key()).set({boardId: newBoardRef.key()})
  }
}

// 配列の中身が同じか検証
function equal(array1, array2) {
  return array1.sort().toString() == array2.sort().toString()
}
