var AppDispatcher = require('../AppDispatcher')

var CurrentArticleConstants = require("../constants/CurrentArticleConstants")

var actionAlias = {
  get: CurrentArticleConstants.CURRENT_ARTICLE_GET,
  set: CurrentArticleConstants.CURRENT_ARTICLE_SET
}

var CurrentArticleActions = {}
Object.keys(actionAlias).map(function (key) {
  CurrentArticleActions[key] = function () {
    AppDispatcher.handleViewAction({
      actionType: actionAlias[key],
      args: arguments
    })
  }
})

module.exports = CurrentArticleActions
