import { FIREBASE_REF_SET, FIREBASE_REBASE_SET } from '../constants/firebaseConstants'
import Firebase from 'firebase'
import Rebase from 're-base'
import deepEqual from 'deep-equal'

export function setFirebaseRef(firebaseUrl) {
  let ref = new Firebase(firebaseUrl)
  return {
    type: FIREBASE_REF_SET,
    value: ref
  }
}

export function setFirebaseRebase(firebaseUrl, teamId) {
  const base = Rebase.createClass(firebaseUrl + '/' + teamId)
  return {
    type: FIREBASE_REBASE_SET,
    value: base
  }
}
export function registerOnlineStatus() {
  return (dispatch, getState) => {
    const { firebaseRef, currentUser } = getState();
    let onlineRef = firebaseRef.child('.info').child('connected')
    let usersRef = firebaseRef.child('taishi_team').child('users')
    onlineRef.on('value', function(snapshot) {
      if (snapshot.val()) {
        // User is online.
        usersRef.child(currentUser.uid).onDisconnect().update({status: 0})
        usersRef.child(currentUser.uid).update({uuid: currentUser.uid, status: 1000})
      } else {
        // User is offline.
        usersRef.child(currentUser.uid).update({status: 0})
      }
    })
  }
}
