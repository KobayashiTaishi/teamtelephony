import assert from 'power-assert'
import counter from '../../src/reducers/counter'
import { INCREMENT_COUNTER, DECREMENT_COUNTER } from '../../src/constants/counterConstants'

describe('reducers', () => {
  describe('counter', () => {
    it('should handle initial state', () => {
      assert(counter(undefined, {}) === 0)
    })

    it('should handle INCREMENT_COUNTER', () => {
      assert(counter(1, { type: INCREMENT_COUNTER }) === 2)
    })

    it('should handle DECREMENT_COUNTER', () => {
      assert(counter(1, { type: DECREMENT_COUNTER }) === 0)
    })

    it('should handle unknown action type', () => {
      assert(counter(1, { type: 'unknown' }) === 1)
    })
  })
})
