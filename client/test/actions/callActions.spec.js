import assert from 'power-assert'
import { applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import * as actions from '../../src/actions/callActions'
import { INCOMING_CALL, OUTGOING_CALL, CONNECT_CALL, DISCONNECT_CALL, STANDARD_TO_GLOBAL, STANDARD_TO_JA, SET_WORKER_CLIENT, SET_ACTIVITY_NAME, SET_PHONE_RINGING, SET_INPUT_NUMBER } from '../../src/constants/callActionTypes'

describe('actions', () => {
  describe('#incoming', () => {
    it('アクションが作られる', () => {
      assert.deepEqual(actions.incoming(),{ type: INCOMING_CALL })
    })
  })
  describe('#outgoing', () => {
    it('アクションが作られる', () => {
      assert.deepEqual(actions.outgoing(),{ type: OUTGOING_CALL })
    })
  })
  describe('#accept', () => {
    it('アクションが作られる', () => {
      assert.deepEqual(actions.accept(),{ type: CONNECT_CALL })
    })
  })
  describe('#decline', () => {
    it('アクションが作られる', () => {
      assert.deepEqual(actions.decline(),{ type: DISCONNECT_CALL })
    })
  })
})
