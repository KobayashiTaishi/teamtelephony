import assert from 'power-assert'
import React from 'react'
import TestUtils from 'react-addons-test-utils'
import Member from '../../src/containers/Member'
import { User } from '../../src/reducers/users'
import { List } from 'immutable'

function setup(propOverrides) {
  let users = List([new User({uid: "idididi", name: "taishi", image: "http://res.cloudinary.com/dkqf51oki/image/upload/l5lngufstt52sxewcbpa.jpg", email: "kbystis@gmail.com"})])

  const props = Object.assign({
    users: users,
    firebaseUrl: "https://sweltering-inferno-9811.firebaseio.com"
  }, propOverrides)

  const component = TestUtils.renderIntoDocument(<Member {...props} />)

  return {
    component: component,
    props: props
  }
}

describe('components', () => {
  describe('Member', () => {
    // it('レンダリングできる', () => {
      // const { component } = setup()
      // TestUtils.isElementOfType(<Member/>, component)
    // }
  })
})
