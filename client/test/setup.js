import { jsdom } from 'jsdom'
import { LocalStorage } from 'node-localstorage'

global.document = jsdom('<!doctype html><html><body></body></html>')
global.window = document.defaultView
global.navigator = global.window.navigator
global.localStorage = new LocalStorage("test")
