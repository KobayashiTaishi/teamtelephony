source 'https://rubygems.org'
ruby '2.2.3'

gem 'rails', '~> 4.2.1'
gem 'responders', '~> 2.0'
gem 'sass-rails', '~> 5.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.1'
gem 'jquery-rails'
gem 'acts-as-taggable-on', '~> 3.4'
# gem 'turbolinks'
gem 'gon', '~> 5.2.3'

gem 'bootstrap-sass', '~> 3.3.5.1'
# gem 'bootstrap-sass-rails'
# gem 'font-awesome-sass-rails'
gem 'bcrypt-ruby', '~> 3.1.2'
# Use unicorn as the app server
gem 'unicorn'
gem 'execjs'
gem 'therubyracer'
# for json
gem 'jbuilder', '~> 1.2'
gem 'oj', '~> 2.9.8'
gem 'rabl'
# for other service api
gem 'twitter', '~> 5.11.0'
gem 'kaminari', '~> 0.16.3'
gem 'rambulance'
gem 'airbrake'
gem 'newrelic_rpm'
gem 'slack-api'
gem 'twilio-ruby', '~> 4.3.0'
gem 'firebase_token_generator'

# 画像
gem 'carrierwave'
gem 'cloudinary', '~> 1.1.0'

# ログ
# 一旦コメントアウト
# gem 'act-fluent-logger-rails'
gem 'lograge'
# gem 'ltsv-logger'

# 認証
gem 'omniauth'
gem 'omniauth-twitter', '~> 1.0.1'

# for Authentication
# バグを踏んだけど最新バージョンでは直っていた。
gem 'sorcery', '~> 0.8.6'
# gem 'sorcery', git: 'git://github.com/NoamB/sorcery.git'
# for api
gem 'doorkeeper', '~> 1.3.0'
# gem "resque", "~> 2.0.0.pre.1", github: "resque/resque"
# gem 'resque', '~> 1.25.2'
# gem 'resque-scheduler', '~> 4.0.0'
# 非同期job
gem "sidekiq"
gem 'sinatra', require: false

# 権限周り https://github.com/CanCanCommunity/cancancan
gem 'cancancan', '~> 1.9.2'
# 管理画面
gem 'rails_admin', '~> 0.6.6'
# ブラウザ
gem 'selenium-webdriver', '~> 2.44.0'
# aws
gem 'aws-sdk', '~>2'
# gem 'omniauth-facebook'
gem 'pg'
# gem 'asset_sync'

# フロントエンド
gem 'bourbon', '~> 4.2.3'
gem 'neat', '~> 1.7.2'

# Use Capistrano for deployment
group :deployment do
  gem 'capistrano', '~> 3.2.1'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rbenv', '~> 2.0.2'
  gem 'capistrano3-unicorn'
end

# for doc
# gem 'sdoc',          group: :doc, require: false

group :development, :test do
  gem 'rest-client'
  gem 'guard', '2.10.5'
  gem 'guard-rspec', '~> 4.5.0'
  gem 'guard-compat', '~> 1.2.1', require: false
  gem 'guard-livereload', '~> 2.4', require: false
  # gem 'guard-rubocop'
  gem 'terminal-notifier-guard'
  gem 'terminal-notifier', '1.6.2'
  gem 'rack-livereload'
  gem 'rspec-rails', '~> 3.0.1'
  gem 'factory_girl_rails', '~> 4.4.1'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/jonleighton/spring
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'ffaker', '~> 2.0.0'
  # debug
  gem 'pry'
  # gem 'pry', '0.9.10'
  gem 'pry-doc'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'pry-remote'
  gem 'http-dump'
  gem 'rubocop', require: false
  gem 'bullet'
  gem 'rails_best_practices', require: false
end

group :production do
  # for batch
  gem 'whenever', require: false
  # for error notification
  gem 'exception_notification', '~> 4.0.1'
  gem 'rb-readline'
end
