class TwilioController < ActionController::Base
  DEFAULT_QUEUE = "yoyoyo"

  def incoming
    user = User.find_by(email: "admin@campus.com")
    number = user.uid
    response = Twilio::TwiML::Response.new do |r|
      r.Dial :callerId => "+81345402452" do |d|
        if /^[\d\+\-\(\) ]+$/.match(number)
          d.Number(CGI::escapeHTML number)
        else
          d.Client number
        end
      end
    end
    render xml: response.text
  end

  def outgoing
    number = params[:PhoneNumber]
    response = Twilio::TwiML::Response.new do |r|
      r.Dial :callerId => "+81345402452" do |d|
        if /^[\d\+\-\(\) ]+$/.match(number)
          d.Number(CGI::escapeHTML number)
        else
          d.Client number
        end
      end
    end
    render xml: response.text
  end

  def enqueue
    response = Twilio::TwiML::Response.new do |r|
      # params[:Digits] == "1" ? queue_name = "カスタマーサービス" :
      queue_name = DEFAULT_QUEUE
      r.Say "コールが#{queue_name}キューにはいりました。", voice: 'woman', language: 'ja-JP'
      # r.Redirect base_url + "/twilio/enqueue"
      r.Enqueue queue_name, waitUrl: request.protocol + request.host_with_port + "/twilio/wait_enqueue"
      r.Redirect request.protocol + request.host_with_port + "/twilio/incoming"
    end
    render xml: response.text
  end

  def wait_enqueue
    response = Twilio::TwiML::Response.new do |r|
      queue_name = params[:Queue] || DEFAULT_QUEUE
      if (true)
        r.Leave
      else
        r.Play "http://com.twilio.sounds.music.s3.amazonaws.com/MARKOVICHAMP-Borghestral.mp3"
      end
    end
    render xml: response.text
  end

  def queue
    response = Twilio::TwiML::Response.new do |r|
      queue_name = params[:Queue] || DEFAULT_QUEUE
      r.Say "#{queue_name}のキューに接続しました。", voice: 'woman', language: 'ja-JP'
      r.Dial do |d|
        d.Queue queue_name
      end
    end
    render xml: response.text
  end

  def collect_digit
    base_url = request.protocol + request.host_with_port

    response = Twilio::TwiML::Response.new do |r|
      # 1桁の数字を取得し（numDigits）、actionのTwiMLに渡す。タイムアウトは10秒。
      r.Gather(action: enqueue_twilio_index_path, numDigits: 1, timeout: 10) do
        r.Say('お問い合わせの場合は１、注文の場合は２を押してください', voice: 'woman', language: 'ja-JP')
      end
      # タイムアウト時の処理
      r.Say('何も入力されませんでした', voice: 'woman', language: 'ja-JP')
      r.Redirect base_url + "/twilio/enqueue"
    end
    render xml: response.text
  end

  def workflow
    message = "Thanks for calling Phil's Company support line." \
              "One of our agents will speak with you soon."
    response = Twilio::TwiML::Response.new do |t|
      t.Say message, voice: "alice", language: "en-GB"
      t.Enqueue workflowSid: Rails.application.secrets.twilio["workflow_sid"]
    end
    render xml: response.text
  end

  def assignment
  end
end
