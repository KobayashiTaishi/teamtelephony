class UsersController < ApplicationController
  doorkeeper_for :show

  def index
    render json: "hoge"
  end

  def new
    @user = User.new
  end

  def show
    render json: current_user
  end

  def create
    @user = User.new(user_params)
    if @user.save
      head :created
      # redirect_to root_url, :notice => "Signed up!"
    else
      head :bad_request
      # render :new
    end
  end

  private

    def user_params
      params.require(:user).permit(:id, :email, :password, :password_confirmation)
    end
end
