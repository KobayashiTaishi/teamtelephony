# sample api
module Api::Versions
  class UsersController < BaseController
    doorkeeper_for :index, :show, :destroy
    # before_filter :validate_token
    # before_filter :set_parameters
    # skip_before_filter :verify_authenticity_token # allow CSRF

    def index
      # deal with api version 1
      case params[:versions]
      when 'v1'
        render json: current_user
      else
        return head(401)
      end
      # Todo bad request
    end

    def create
      case params[:versions]
      when 'v1'
        @user = User.new(user_params)
      else
        return head(401)
      end

      if @user.save
        render action: 'show', status: :created
      else
        head :bad_request
      end
    end

    def show
      render json: current_user
    end

    def destroy
    end

    private

      def user_params
        params.require(:user).permit(:email, :name, :password, :password_confirmation)
      end

    # # def validate_token
    # #   return head(401) unless doorkeeper_token
    # # end
    #
    # def set_parameters
    #   sign_in 'user', User.find(doorkeeper_token.resource_owner_id)
    # end
  end
end
