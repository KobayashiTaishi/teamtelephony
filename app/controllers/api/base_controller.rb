# base controller inherited by all api controllers
class Api::BaseController < ApplicationController
  # respond_to :json
  # before_action :validate_format
  skip_before_action :verify_authenticity_token

  private

  def validate_format
    render nothing: true, status: :not_acceptable unless params[:format] == 'json'
  end
end
