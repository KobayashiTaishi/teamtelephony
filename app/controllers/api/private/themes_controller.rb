module Api::Private
  class ThemesController < BaseController
    before_action :set_theme, only: [:show]
    respond_to :json

    # GET /themes
    # GET /themes.json
    def index
      @themes = Theme.all
    end

    # GET /themes/1
    # GET /themes/1.json
    def show
    end

    # GET /themes/new
    def new
      @theme = Theme.new
    end

    # POST /themes
    # POST /themes.json
    def create
      @theme = Theme.new(theme_params)
      respond_with(@theme)
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_theme
      @theme = Theme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def theme_params
      params.require(:theme).permit(:name, :description, :main_image)
    end
  end
end
