module Api::Private
  class CategoriesController < BaseController
    before_action :set_category, only: [:show]
    respond_to :json

    # GET /themes
    # GET /themes.json
    def index
      @categories = Category.all
    end

    # GET /categories/1
    # GET /categories/1.json
    def show
    end

    # GET /categories/new
    def new
      @category = Category.new
    end

    # POST /categories
    # POST /categories.json
    def create
      @category = Category.new(category_params)
      respond_with(@category)
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :description, :main_image)
    end
  end
end
