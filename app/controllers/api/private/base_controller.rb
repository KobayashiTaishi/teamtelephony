# base class in private api
class Api::Private::BaseController < Api::BaseController
  before_action :require_login
end
