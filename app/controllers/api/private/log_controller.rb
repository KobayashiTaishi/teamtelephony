module Api::Private
  class LogController < BaseController
    respond_to :json

    def event
      head :ok
    end
  end
end
