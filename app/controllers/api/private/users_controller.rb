module Api::Private
  class UsersController < BaseController
    respond_to :json
    def index
      @users = User.filter_team(current_user.team_id)
    end
  end
end
