# sample api
module Api::Private
  class ArticlesController < BaseController
    before_action :set_article, only: [:show, :edit, :update, :destroy]
    respond_to :json

    def index
      @articles = Article.includes(:category)
        .publish
        .order(published_at: :desc)
        .page(params[:page])
        .per(6)
    end

    def show
      respond_with(@article)
    end

    def new
      @article = Article.new
      respond_with(@article)
    end

    def create
      @article = Article.new(article_params)
      @article.user = current_user
      params[:tag_list] ? @article.tag_list = params[:tag_list].map{|tag| tag[:text]} : @article.tag_list = []
      params[:references] ? @article.references = params[:references].map{|tag| tag[:text]} : @article.references = []
      @article.save
      respond_with(@article)
    end

    def update
      @article.user = current_user if @article.user && @article.user.writer?
      params[:tag_list] ? @article.tag_list = params[:tag_list].map{|tag| tag[:text]} : @article.tag_list = []
      params[:references] ? @article.references = params[:references].map{|tag| tag[:text]} : @article.references = []
      @article.update(article_params)
      notify_to_slack(@article.user, @article) if @article.user && @article.user.writer?
      respond_with(@article)
    end

    def tags
      @tags = Article.tags_on(:tags).where("name LIKE ?", "%#{params[:query]}%")
    end

    # imageをupload
    # Todo ファイル名を変更
    def upload_image
      file = params[:file]
      thumb = Magick::Image.from_blob(file.read).shift
      thumb_out = thumb.to_blob
      path = "images/contents/" + rand(36**10).to_s(36) + ".png"
      s3 = Aws::S3::Client.new
      s3.put_object(
        bucket: Rails.application.secrets.aws_bucket,
        # body: file.tempfile,
        body: thumb_out,
        key: path,
        acl: 'public-read'
      )
      @image = 'https://s3-ap-northeast-1.amazonaws.com/' + Rails.application.secrets.aws_bucket + '/' + path
    end

    def upload_image_url
      file = Magick::Image.read(params[:url])[0]
      thumb_out = file.to_blob
      path = "images/contents/" + rand(36**10).to_s(36) + ".png"
      s3 = Aws::S3::Client.new
      s3.put_object(
        bucket: Rails.application.secrets.aws_bucket,
        # body: file.tempfile,
        body: thumb_out,
        key: path,
        acl: 'public-read'
      )
      @image = 'https://s3-ap-northeast-1.amazonaws.com/' + Rails.application.secrets.aws_bucket + '/' + path
      render "upload_image"
    end

    # リサイズしたimageをupload
    # Todo ファイル名を変更
    def upload_crop_image
      size = JSON.parse(params[:size_data])
      file = params[:file]
      thumb = Magick::Image.from_blob(file.read).shift
      thumb = thumb.crop(size["x"].to_i, size["y"].to_i, size["width"].to_i, size["height"].to_i)
      thumb = thumb.resize_to_fit(params["width"].to_i, params["height"].to_i)
      thumb_out = thumb.to_blob
      path = "images/main_image/" + rand(36**10).to_s(36) + ".png"
      s3 = Aws::S3::Client.new
      s3.put_object(
        bucket: Rails.application.secrets.aws_bucket,
        # body: file.tempfile,
        body: thumb_out,
        key: path,
        acl: 'public-read'
      )
      @image = 'https://s3-ap-northeast-1.amazonaws.com/' + Rails.application.secrets.aws_bucket + '/' + path
      render "upload_image"
    end

    private

      def notify_to_slack(user, article)
        return unless Rails.env.production?
        text = <<-EOC
        [#{user.name}]様より 新しい記事が投稿されました。

        ▼タイトル
        #{article.title}
        ▼URL
        http://qolony.jp/#{article.id}
        ▼ユーザーさんのメールアドレス
        #{user.email}
         EOC

        Slack.chat_postMessage text: text, username: "美人社長秘書", channel: "#article"
      end

      def set_article
        @article = Article.find(params[:id])
      end

      def article_params
        params.require(:article).permit(
          :title,
          :description,
          :main_image,
          :tag_list,
          :theme_id,
          :category_id,
          :thumbnail_image,
          :sub_url,
          :theme_id,
          :status,
          :user_id,
          contents: [objects: [
            :text,
            :type,
            :image,
            :src,
            :url,
            :start,
            questions: [
              :image,
              :title,
              credit: [:text, :url],
             answer_information: [
               :column_type,
               answers: [
                 :image,
                 :title,
                 :correct,
                 :result_id,
                 credit: [:text, :url]
               ]
             ],
             each_result: [
               :explanation,
               :image,
               credit: [:text, :url]
             ]
            ],
            results: [
              :result_id,
              :title,
              :image,
              :twitter_image,
              :description,
              :to,
              :from,
              credit: [:text, :url]
            ]
          ]])
      end
  end
end
