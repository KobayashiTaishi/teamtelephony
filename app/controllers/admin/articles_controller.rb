module Admin
  class ArticlesController < BaseController
    authorize_resource only: [:index, :user, :publish, :completion, :future, :draft, :new, :create, :without_image]
    load_and_authorize_resource only: [:show, :edit, :update, :destroy, :to_completion, :reservation_post, :to_idea, :to_without_image]

    respond_to :html

    def index
      @articles = Article.all
        .eager_load(:user)
        .eager_load(:theme)
        .order(updated_at: :desc)
        .page(params[:page])
      respond_with(@articles)
    end

    def attentions
      @article = Article.new
      @articles = Article.where(attention: true)
        .order(updated_at: :desc)
        .page(params[:page])
      respond_with(@articles)
    end

    def add_attention
      article = Article.find_by(id: params[:article][:id])
      article.update(attention: true)
      redirect_to :back
    end

    def delete_attention
      article = Article.find_by(id: params[:id])
      article.update(attention: false)
      redirect_to :back
    end

    def show
      respond_with(@article)
    end

    def user
      if params[:user_id].present?
        user_id = params[:user_id]
      else
        user_id = current_user.id
      end

      @articles = Article
        .filter_user(user_id)
        .eager_load(:user)
        .eager_load(:theme)
        .order(updated_at: :desc)
        .page(params[:page])
      render :index
    end

    def publish
      @articles = Article
        .publish
        .eager_load(:user)
        .eager_load(:theme)
        .order(published_at: :desc)
        .page(params[:page])
      render :index
    end

    def draft
      @articles = Article
        .draft
        .eager_load(:user)
        .eager_load(:theme)
        .order(updated_at: :desc)
        .page(params[:page])
      render :index
    end

    def future
      @articles = Article
        .future
        .eager_load(:user)
        .eager_load(:theme)
        .order(updated_at: :desc)
        .page(params[:page])
      render :index
    end

    def completion
      @articles = Article
        .completion
        .eager_load(:user)
        .eager_load(:theme)
        .order(updated_at: :desc)
        .page(params[:page])
      render :index
    end

    def without_image
      @articles = Article
        .without_image
        .eager_load(:user)
        .eager_load(:theme)
        .order(updated_at: :desc)
        .page(params[:page])
      render :index
    end

    def new
      @article = Article.new
      respond_with(@article)
    end

    def edit
    end

    def to_completion
      @article.completion!
      redirect_to :back
    end

    def to_idea
      @article.draft!
      @article.update(user_id: nil)
      redirect_to :back
    end

    def to_without_image
      @article.without_image!
      @article.update(user_id: nil)
      redirect_to :back
    end

    def reservation_post
      # Todo 実装する
      raise
      # redirect_to :back
    end

    def create
      @article = Article.new(article_params)
      @article.save
      respond_with(@article)
    end

    def update
      @article.update(article_params)
      respond_with(@article)
    end

    def destroy
      @article.destroy
      respond_with(@article)
    end

    private

    def set_article
      @article = Article.find(params[:id])
    end

    def article_params
      params.require(:article).permit(:title, :description, :main_image, :sub_url, :theme_id, :status, :user_id, contents: [:text, :type, :image, :src])
    end
  end
end
