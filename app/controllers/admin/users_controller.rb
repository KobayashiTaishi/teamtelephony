module Admin
  class UsersController < BaseController
    authorize_resource
    respond_to :html

    def index
      @users = User
        .all
        .order(updated_at: :desc)
        .page(params[:page])
      respond_with(@users)
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)
      if @user.save
        @user.writer!
        redirect_to admin_users_path, notice: "ライターを追加しました!"
      else
        redirect_to :back, notice: @user.errors.full_messages
      end
    end

    private

      def user_params
        params.require(:user).permit(:id, :email, :name, :password, :password_confirmation)
      end
  end
end
