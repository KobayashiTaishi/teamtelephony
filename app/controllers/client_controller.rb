class ClientController < ApplicationController
  before_action :require_login, only: [:launch]

  def launch
    @user = current_user
    secrets = Rails.application.secrets
    generator = Firebase::FirebaseTokenGenerator.new(secrets.firebase_key)
    @firebase_token = generator.create_token({ uid: @user.uid, auth_data: "foo", other_auth_data: "bar" })
    capability = Twilio::Util::Capability.new secrets.twilio["account_sid"], secrets.twilio["auth_token"]
    capability.allow_client_outgoing secrets.twilio["outgoing_sid"]
    capability.allow_client_incoming @user.uid
    @capability_token = capability.generate

    @twilio_client = Twilio::REST::Client.new
    @phone_numbers = @twilio_client.account.incoming_phone_numbers.list.map(&:phone_number)
    # デモのワーカー
    worker_sid = 'WK3c7ddaedc512e390563e056c060e2348'
    worker_capability = Twilio::TaskRouter::WorkerCapability.new Rails.application.secrets.twilio["account_sid"], Rails.application.secrets.twilio["auth_token"], Rails.application.secrets.twilio["workspace_sid"], worker_sid
    worker_capability.allow_activity_updates
    worker_capability.allow_reservation_updates
    @worker_token = worker_capability.generate_token
    @users = User.filter_team(current_user.team_id)
    gon.jbuilder
  end
end
