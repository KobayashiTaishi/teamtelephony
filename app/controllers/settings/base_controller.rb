class Settings::BaseController < ApplicationController
  layout "settings"
  before_action :require_login

  def index
    request.subdomain
  end
end
