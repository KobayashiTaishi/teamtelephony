class Twilio::TaskRouterController < ActionController::Base

  def workflow
    task_attributes = {}
    # task_attributes[:status] = "Gold" if true
    task_attributes[:selected_language] = 'es'
    message = "One of our agents will speak with you soon."
    response = Twilio::TwiML::Response.new do |t|
      t.Say message, voice: "alice", language: "en-GB"
      t.Enqueue workflowSid: Rails.application.secrets.twilio["workflow_sid"] do |e|
        e.TaskAttributes task_attributes.to_json
      end
    end
    render xml: response.text
  end

  # {"AccountSid"=>
  #  "ACee0f128151c1a72f194fb66cbd25b0bd",
  # "WorkspaceSid"=>
  #  "WSf1ceb9e814d690b4788d1837f9c18af9",
  # "WorkflowSid"=>
  #  "WW102785a7f5525476c3230a6fddc49db5",
  # "ReservationSid"=>
  #  "WRb532d649afcfc7b81229684105955780",
  # "TaskQueueSid"=>
  #  "WQ65c048f95844ee86f7f22a3923d1c5a4",
  # "TaskSid"=>
  #  "WT2499b9f33b830f01761bed5c4c766a1f",
  # "WorkerSid"=>
  #  "WK3c7ddaedc512e390563e056c060e2348",
  # "TaskAge"=>"0",
  # "TaskPriority"=>"1",
  # "TaskAttributes"=>
  # "{
  #   "from_country":"JP",
  #   "called":"+81345402452",
  #   "selected_language":"es",
  #   "to_country":"JP",
  #   "to_city":"",
  #   "to_state":"Tokyo",
  #   "caller_country":"JP",
  #   "call_status":"in-progress",
  #   "call_sid":"CA1f2d736b235535b31be88370082c9c35",
  #   "account_sid":"ACee0f128151c1a72f194fb66cbd25b0bd",
  #   "from_zip":"",
  #   "from":"+81345402452",
  #   "direction":"inbound",
  #   "called_zip":"",
  #   "caller_state":"Tokyo",
  #   "to_zip":"",
  #   "called_country":"JP",
  #   "from_city":"",
  #   "called_city":"",
  #   "caller_zip":"",
  #   "api_version":"2010-04-01",
  #   "called_state":"Tokyo",
  #   "from_state":"Tokyo",
  #   "caller":"+81345402452",
  #   "caller_city":"",
  #   "to":"+81345402452"
  # }",
  # "WorkerAttributes"=>
  #  "{"languages":["en","es"]}",
  # "controller"=>"twilio/task_router",
  # "action"=>"assignment"
  def assignment
    respond_to :json
  end

  def create_task
    client = Twilio::REST::TaskRouterClient.new(nil, nil, Rails.application.secrets.twilio["workspace_sid"])
    task = client.workspace.tasks.create(workflow_sid: "WW102785a7f5525476c3230a6fddc49db5", attributes: '{"selected_language":"es"}')
    task.attributes
  end

  def accept_reservation
    task_sid = params[:task_sid]
    client = Twilio::REST::TaskRouterClient.new(nil, nil, Rails.application.secrets.twilio["workspace_sid"])
    reservation_sid = params[:reservation_sid]
    reservation = client.workspace.tasks.get(task_sid).reservations.get(reservation_sid)
    reservation.update(reservationStatus: 'accepted')
    reservation.worker_name
  end

  def agent
    # Alice
    worker_sid = 'WK3c7ddaedc512e390563e056c060e2348'
    worker_capability = Twilio::TaskRouter::WorkerCapability.new Rails.application.secrets.twilio["account_sid"], Rails.application.secrets.twilio["auth_token"], Rails.application.secrets.twilio["workspace_sid"], worker_sid
    worker_capability.allow_activity_updates
    worker_capability.allow_reservation_updates
    @worker_token = worker_capability.generate_token
    render layout: false
  end
end
