class ArticlesController < ApplicationController
  before_action :set_article, only: [:show]

  def index
    @articles = Article.includes(:category)
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder}
      format.json
    end
  end

  def attentions
    @articles = Article.includes(:category)
      .where(attention: true)
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder template: "app/views/articles/index.json.jbuilder"}
      format.json { render "articles/index" }
    end
  end

  def tag
    @tag = params[:name]
    @articles =Article.tagged_with(@tag)
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder}
      format.json { render "articles/index" }
    end
  end

  def quiz
    @articles = Article.quiz
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder template: "app/views/articles/index.json.jbuilder"}
      format.json { render "articles/index" }
    end
  end

  def lifestyle
    @articles = Article.lifestyle
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder template: "app/views/articles/index.json.jbuilder"}
      format.json {render "articles/index"}
    end
  end

  def culture
    @articles = Article.culture
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder template: "app/views/articles/index.json.jbuilder"}
      format.json {render "articles/index"}
    end
  end

  def entertainment
    @articles = Article.entertainment
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder template: "app/views/articles/index.json.jbuilder"}
      format.json {render "articles/index"}
    end
  end

  def topic
    @articles = Article.topic
      .publish
      .order(published_at: :desc)
      .page(params[:page])
      .per(6)

    respond_to do |format|
      format.html {gon.jbuilder template: "app/views/articles/index.json.jbuilder"}
      format.json {render "articles/index"}
    end
  end

  def show
    raise NotFoundError unless @article.can_read_user?(current_user)
    gon.jbuilder
    respond_to do |format|
      format.html
      format.json
    end
  end

  def js_show
    @article = Article.find(params[:id])
    gon.jbuilder
  end

  private

  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :description, :main_image, :sub_url, :theme_id, :status, :user_id, :name, contents: [:text, :type, :image, :src])
  end
end
