class ApplicationController < ActionController::Base
  include Errors
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # def current_user
  #   @current_user ||= User.find_by_id(doorkeeper_token.resource_owner_id) if doorkeeper_token
  # end

  def append_info_to_payload(payload)
    super
    payload[:host] = request.host
    payload[:ua] = request.user_agent
    payload[:request] = request.method + " " + request.path
    payload[:cookie] = request.cookies["_Telott_session"]
    payload[:referer] = request.env["HTTP_REFERER"]
    request.params.each do |key, value|
      payload[key.to_sym] = value
    end
  end

  private

  # sorceryでログインできなかったとき
  def not_authenticated
    redirect_to login_path
    # raise CanCan::AccessDenied
  end

  def only_development
    not_authenticated unless Rails.env.development?
  end
end
