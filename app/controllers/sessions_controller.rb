class SessionsController < ApplicationController
  before_action :only_development, only: [:debug]
  before_action :require_login, only: [:launch]

  include CloudinaryHelper

  def launch
    @user = current_user
    secrets = Rails.application.secrets
    generator = Firebase::FirebaseTokenGenerator.new(secrets.firebase_key)
    @firebase_token = generator.create_token({ uid: @user.uid, auth_data: "foo", other_auth_data: "bar" })
    capability = Twilio::Util::Capability.new secrets.twilio["account_sid"], secrets.twilio["auth_token"]
    capability.allow_client_outgoing secrets.twilio["outgoing_sid"]
    capability.allow_client_incoming @user.uid
    @capability_token = capability.generate

    # デモのワーカー
    worker_sid = 'WK3c7ddaedc512e390563e056c060e2348'
    worker_capability = Twilio::TaskRouter::WorkerCapability.new Rails.application.secrets.twilio["account_sid"], Rails.application.secrets.twilio["auth_token"], Rails.application.secrets.twilio["workspace_sid"], worker_sid
    worker_capability.allow_activity_updates
    worker_capability.allow_reservation_updates
    @worker_token = worker_capability.generate_token
    @users = User.filter_team(current_user.team_id)
    gon.jbuilder
  end
  # info
  #  {"nickname"=>"taish_ja",
  # "name"=>"KobayashiTaishi",
  # "location"=>"",
  # "image"=>
  #  "http://pbs.twimg.com/profile_images/378800000671725557/d1328b6b332c670047e35e064a36992a_normal.jpeg",
  # "description"=>"生粋のエンジニア",
  # "urls"=>
  #  {"Website"=>nil,
  #   "Twitter"=>"https://twitter.com/taish_ja"}}
  def callback
    auth_params = request.env['omniauth.auth']
    case auth_params['provider']
    when "twitter"
      auth = Auth.find_or_initialize_by(provider: auth_params['provider'], uid: auth_params['uid'])
      if auth.user.nil?
        # 加工前の写真を使う
        image_url = auth_params['info']['image'].gsub("_normal.jpeg", ".jpeg")
        # アップロード
        cloudinary = Cloudinary::Uploader.upload(image_url,  eager: { quality: "jpegmini", width: 94, height: 94, crop: :thumb }, faces: true)
        user = User.create(name: auth_params['info']['name'], avatar: cloudinary["eager"][0]["url"])
        user.auths << auth
      else
        user = auth.user
      end
    end
    auto_login(user)
    redirect_to admin_root_path
  end

  def twitter_login
    render layout: 'login'
  end

  def new
    render layout: 'login'
  end

  def signup
    render layout: 'login'
  end

  def demo_first_user
    auto_login Team.first.users.first
    redirect_to root_path
  end

  def demo_second_user
    auto_login Team.first.users.second
    redirect_to root_path
  end

  def demo_third_user
    auto_login Team.first.users.third
    redirect_to root_path
  end

  def create_acount
    @user = User.new(email: params[:email], password: params[:password], password_confirmation: params[:password_confirmation])
    if @user.save
      @user.writer!
      auto_login(@user)
      redirect_to admin_root_path, notice: "アカウントが作成されました"
    else
      redirect_to :back, notice: @user.errors.full_messages
    end
  end

  # jsのデバッグ用
  def debug
    # This application sid will play a Welcome Message.
    demo_app_sid = 'APabe7650f654fc34655fc81ae71caa3ff'
    capability = Twilio::Util::Capability.new Rails.application.secrets.twilio["account_sid"], Rails.application.secrets.twilio["auth_token"]
    capability.allow_client_outgoing demo_app_sid
    @token = capability.generate
    gon.jbuilder
    render layout: 'debug'
  end

  def create
    user = login(params[:email], params[:password], params[:remember_me])
    if user
      redirect_back_or_to root_path
    else
      redirect_to :back, notice: "Email or password was invalid."
    end
  end

  def destroy
    logout
    redirect_to login_path, notice: "Logged out!"
  end
end
