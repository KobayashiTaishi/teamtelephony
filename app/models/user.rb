class User < ActiveRecord::Base
  authenticates_with_sorcery!
  enum status: [:normal, :writer, :admin, :editor]
  has_many :articles
  has_many :auths
  belongs_to :team
  scope :filter_team, -> (team_id) { where(team_id: team_id) }

  validates :password, length: { minimum: 6 }, unless: proc { |a| a.password.blank? }
  validates :password, confirmation: true, unless: proc { |a| a.password.blank? }
  validates :password_confirmation, presence: true, unless: proc { |a| a.password.blank? }
  validates :email, uniqueness: true, if: proc { |a| a.team.users.exists?(email: a.email) }
  validates :name, uniqueness: true, if: proc { |a| a.name.present? }

  # twilioに登録するIDはハイフンが使えないのでアンダースコアに変換する
  def uid
    uuid.gsub("-", "_") unless uuid.nil?
  end

  # def uuid
  #   return nil if super.nil?
  #   super.gsub("-", "_")
  # end
end
