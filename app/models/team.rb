class Team < ActiveRecord::Base
  has_many :users
  has_many :phone_numbers
  validates :name, uniqueness: true
end
