class Article < ActiveRecord::Base
  include Errors
  # 左から下書き、公開、完成してるけどまだ公開しない、完成済み
  enum status: [:draft, :publish, :future, :completion, :without_image]

  validates :title, presence: true
  validates :description, presence: true
  validates :category, presence: true

  belongs_to :user
  belongs_to :category
  has_and_belongs_to_many :lists
  acts_as_taggable

  scope :until_idea, -> { where(user_id: nil) }

  scope :entertainment, -> {
    includes(:category).where(categories: {name: "エンターテイメント"})
  }
  scope :culture, -> {
    includes(:category).where(categories: {name: "カルチャー"})
  }
  scope :topic, -> {
    includes(:category).where(categories: {name: "話題"})
  }
  scope :lifestyle, -> {
    includes(:category).where(categories: {name: "ライフスタイル"})
  }
  scope :quiz, -> {
    includes(:theme).where(themes: {name: "クイズ"})
  }

  scope :filter_user, -> (user_id) { where(user_id: user_id) }

  def can_read_user?(user)
    # 公開記事
    return true if self.publish?
    # ログインしていない
    return false if user.nil?

    if user.admin? || user.editor?
      return true
    elsif user.writer?
      if self.user == user
        return true
      # 記事案ならみれる
      elsif self.user.nil?
        return true
      else
        return false
      end
    else
      return false
    end
  end
end
