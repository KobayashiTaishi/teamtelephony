class UploadImageJob < ActiveJob::Base
  queue_as :image

  def perform(body, path)
    s3 = Aws::S3::Client.new
    s3.put_object(
      bucket: Rails.application.secrets.aws_bucket,
      body: body.tempfile,
      key: path,
      acl: 'public-read'
    )
  end
end
