json.team do
  json.uuid @user.team.uuid
  json.phone_numbers @phone_numbers
end
json.setting do
  json.env Rails.env
  json.firebase_url Rails.env.production? ? "https://production.firebaseio.com" : "https://sweltering-inferno-9811.firebaseio.com"
end
json.current_user do
  json.extract! @user, :name, :email, :created_at, :updated_at
  json.uid @user.uid
  json.capability_token @capability_token
  json.firebase_token @firebase_token
  json.worker_token @worker_token
end
json.users(@users) do |user|
  json.extract! user, :name, :email
  json.uid user.uid
  json.image controller.view_context.cl_image_path("l5lngufstt52sxewcbpa.jpg")
end
