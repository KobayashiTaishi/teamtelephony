json.array!(@articles) do |article|
  json.tag @tag
  json.extract! article, :id, :title, :description, :thumbnail_image, :main_image
  json.category article.category.name
  json.published_at article.published_at.present? ? article.published_at.to_date.strftime("%Y.%m.%d") : nil
end
