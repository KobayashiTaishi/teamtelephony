json.extract! @article, :id, :title, :description, :main_image, :thumbnail_image, :sub_url, :theme_id, :status, :user_id, :created_at, :updated_at, :references
json.contents @article.contents["objects"] ? @article.contents : {objects: []}
json.category @article.category.name
json.tag_list @article.tag_list.to_a
json.published_at @article.published_at.present? ? @article.published_at.to_date.strftime("%Y.%m.%d") : nil
json.url "http://qolony.jp/#{@article.id}"
