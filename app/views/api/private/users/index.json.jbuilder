json.array!(@users) do |user|
  json.extract! user, :name, :email
  json.uid user.uid
  json.image controller.view_context.cl_image_path("l5lngufstt52sxewcbpa.jpg")
end
