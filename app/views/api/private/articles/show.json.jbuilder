json.extract! @article, :id, :title, :description, :main_image, :thumbnail_image, :sub_url, :contents, :theme_id, :category_id, :status, :user_id, :created_at, :updated_at
json.tag_list @article.tag_list do |tag|
  json.text tag
end
json.references @article.references do |reference|
  json.text reference
end
