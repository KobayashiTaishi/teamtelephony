json.array!(@themes) do |theme|
  json.extract! theme, :id, :name, :description, :main_image
  json.url theme_url(theme, format: :json)
end
