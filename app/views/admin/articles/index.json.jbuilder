json.array!(@articles) do |article|
  json.extract! article, :id, :title, :description, :sub_url, :contents, :theme_id, :status, :user_id
  json.url article_url(article, format: :json)
end
