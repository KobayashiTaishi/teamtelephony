module ArticlesHelper
  # contentを展開
  def show_content(content)
    case content["type"]
    when "text"
      content_tag(:p, content["text"], class: :content_p)
    when "headline"
      content_tag(:h2, content["text"], class: :content_h2)
    when "reference"
      content_tag(:blockquote, content["text"], class: :content_blockquote)
    when "image"
      image_tag(content["image"], class: :content_img)
    when "youtube"
      youtube_tag(content["src"]).html_safe
    when "script"
      content["src"].html_safe
    when "tirivia"
      content_tag(:div, content["questions"], class: :trivia) #実際の記事表示で必要 show.html.erb
    when "personality"
      content_tag(:div, content["questions"], class: :personality)
    end
  end

  def youtube_tag(src)
    return '<div class="youtube-container"><iframe id="player" type="text/html" width="640" height="390" src=' + src + ' frameborder="0"></iframe></div>'
  end

  def status_button(article)
    if  article.publish?
      return '<span class="label label-primary label-mini btn-round">公開</span>'
    elsif article.future?
      return '<span class="label label-success label-mini btn-round">公開予定</span>'
    elsif article.draft?
      return '<span class="label label-warning label-mini btn-round">下書き</span>'
    elsif article.completion?
      return '<span class="label label-info label-mini btn-round">完成済み</span>'
    end
  end
end
