# コントローラー全体で使うメソッドなどをまとめたモジュール
module ApplicationHelper
  def worker_url(asset)
    if Rails.env.production?
      "http://qolony.jp/" + javascript_path(asset)
    else
      javascript_url asset
    end
  end
  def og_url(page_url)
    base_url = "http://qolony.jp"
    if page_url.empty?
      base_url
    else
      page_url
    end
  end

  def og_image(page_image)
    base_image = "https://s3-ap-northeast-1.amazonaws.com/qolony/images/service/cover5.png"
    if page_image.empty?
      base_image
    else
      page_image
    end
  end

  def full_description(page_description)
    # base_description = "QOLONY（コロニー）はスキマ時間も休日も、「面白さ」＋「遊び心」をあなたに届けます。クイズや診断をみんなでシェアする、読み物を超えたエンタメマガジンです"
    base_description = "コールセンターアプリプロトタイプ"
    if page_description.empty?
      base_description
    else
      page_description
    end
  end

  def full_title(page_title)
    base_title = "コールセンターアプリプロトタイプ"
    # base_title = "QOLONY（コロニー） | クイズや診断、遊び心が満載の次世代エンタメマガジン"
    if page_title.empty?
      base_title
    else
      page_title
    end
  end

  def gulp_asset_path(name)
    # Todo ローンチ前には書く
    # if Rails.env.production? || Rails.env.staging?
    #   name = Gyazo::Assets.manifest[name]
    # end

    "/dist/#{name}"
  end
end
