var gulp = require("gulp")
var gutil = require("gulp-util")
// vinylオブジェクトにする
var source = require("vinyl-source-stream")
var buffer = require("vinyl-buffer")
// 圧縮
var uglify = require("gulp-uglify")
var size = require("gulp-size")
var sourcemaps = require("gulp-sourcemaps")
// モジュール管理
var gReact = require("gulp-react")
// bower
// var debowerify = require("debowerify")
// react
// var reactify = require ("reactify")
// bower
// require("debowerify")
// react
// 監視
// ES6
// var to5ify = require("6to5ify")
var babel = require("gulp-babel")
var using = require("gulp-using")
// デバッグ
var debug = require("gulp-debug")
var rename = require("gulp-rename")


// function bundleExec(bundler) {
//   // var b = watchify(browserify(jsFiles(), watchify.args))
//   return bundler
//   .bundle()
//   .on("error", function (error) {
//     gutil.log("Browserify Error", error.message)
//     this.emit("end")
//   })
//   // .on("error", gutil.log.bind(gutil, "Browserify Error"))
//   .pipe(source("bundle.js"))
//   .pipe(debug({title : "bundleExec"}))
//   .pipe(buffer())
//   .pipe(uglify())
//   .pipe(size())
//   .pipe(sourcemaps.init({loadMaps : true})) // loads map from browserify file
//   .pipe(sourcemaps.write("./")) // writes .map file
//   .pipe(gulp.dest("../public/dist"))
// }
//
// function bundlee() {
//   var bundler = watchify(browserify({
//     entries : ["./compile_src/init.js"], // The entry file, normally "main.js"
//     transform : [
//       ["reactify", {"es6" : true}]
//     ],
//     // transform : ["reactify"], // Convert JSX style
//     debug : true,
//     cache : {},
//     packageCache : {},
//     fullPaths : true
//   }))
//
//   bundler.on("update", function() {
//     bundleExec(bundler)
//   }) // on any dep update, runs the bundler
//
//   gutil.log("Bundle reload")
//   bundleExec(bundler)
//   gutil.log("Bundle finish")
// }
//
// gulp.task("watch:js", ["build"], function() {
//   gulp.watch("./compile_src/**/*{.js}", function() {
//     bundlee()
//   })
// })
//
// gulp.task("bundle", ["compile"], function() {
//     bundlee()
// })

// function jsFiles() {
//   return glob.sync("./src/**/*{.js, .jsx}")
//       .map(function(name) { path.join(process.cwd(), name)})
//
// }

// function compileJsx() {
//   return gulp.src("./src/**/*.jsx")
//     .pipe(debug({title : "compile jsx"}))
//     .pipe(gReact({harmony : true}))
//     .on("error", function (error) {
//       gutil.log("Browserify Error", error.message)
//       this.emit("end")
//     })
//     .pipe(gulp.dest("./compile_src"))
// }
//
// function compileEs6() {
//   return gulp.src("./src/**/*.es6")
//              .pipe(debug({title : "build es6"}))
//              .pipe(babel())
//              .on("error", function (error) {
//                gutil.log("Browserify Error", error.message)
//                this.emit("end")
//              })
//              .pipe(using())
//              .pipe(rename({
//                extname : ".js"
//              }))
//              .pipe(gulp.dest("./compile_src"))
// }
//
// gulp.task("compile:es6", function() {
//   gulp.watch("./src/**/*.es6", function() {
//     compileEs6()
//   })
// })
//
// gulp.task("compile:jsx", function() {
//   gulp.watch("./src/**/*.jsx", function() {
//     compileJsx()
//   })
// })

//
// gulp.task("clean", function (done) {
//   del(["dist/"], done);
// });
//
// gulp.task("build", function() {
//     return browserify({
//             entries : ["./compile_src/app.js"],
//             // transform: ["reactify", "debowerify", "6to5ify"]
//             transform : ["reactify"]
//         })
//         .bundle()
//         .on("error", function (error) {
//           gutil.log("Browserify Error", error.message)
//           this.emit("end")
//         })
//         .pipe(source("./bundle.js"))
//         .pipe(buffer())
//         .pipe(uglify())
//         .pipe(size())
//         .pipe(gulp.dest("dist/"))
// })
