var gulp = require("gulp")
var mocha = require("gulp-mocha")
var gutil = require("gulp-util")
var babel = require("babel/register")
// var using = require("gulp-using")
// var rename = require("gulp-rename")
// var debug = require("gulp-debug")
// var mochify = require("mochify")
//
// function compileTestEs6() {
//   return gulp.src(["src/**/*.spec.es6"])
//     .pipe(debug({title : "compile test es6"}))
//     .pipe(babel())
//     .on("error", function (error) {
//       gutil.log("Browserify Error", error.message)
//       this.emit("end")
//     })
//     .pipe(using())
//     .pipe(rename({
//       extname : ".js"
//     }))
//     .pipe(gulp.dest("./compile_src"))
// }
//
// gulp.task("compile:test:es6", function () {
//   gulp.watch("src/**/*.spec.es6", function() {
//     compileTestEs6()
//   })
// })
//
gulp.task("mocha", function() {
  return gulp.src(["./client/_test/**/*.spec.js"], { read : false })
    .pipe(mocha({
      reporter : "spec",
      compilers: { js: babel }
    }))
    .on("error", gutil.log)
})
//
gulp.task("watch-mocha", function() {
  gulp.watch([".client/src/**/*.js", "./client/_test/**/*.spec.js"], ["mocha"])
})

// gulp.task("mochify", function() {
//   return mochify("./compile_src/**/*.spec.js", {
//     node : true,
//     reporter : "spec",
//     cover : true
//   })
//   .bundle()
//   .on("error", gutil.log)
// })
// gulp.task('test', ['scripts'], function() {
//  var bowerDeps = wiredep({
//    directory: 'bower_components',
//    exclude: ['bootstrap-sass-official'],
//    dependencies: true,
//    devDependencies: true
//  });

 // var testFiles = bowerDeps.js.concat([
 //   '.tmp/{app,components}/**/!(index).js',
 //   '.tmp/{app,components}/**/index.js',
 //   'src/{app,components}/**/*.spec.js',
 //   'src/{app,components}/**/*.mock.js'
 // ]);

//  return gulp.src(testFiles)
//    .pipe($.karma({
//      configFile: 'karma.conf.js',
//      action: 'run'
//    }))
//    .on('error', function(err) {
//      // Make sure failed tests cause gulp to exit non-zero
//      throw err;
//    });
// });
