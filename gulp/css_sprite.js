var gulp = require("gulp")
var spritesmith = require("gulp.spritesmith")

gulp.task("sprite", function () {
  var spriteData = gulp.src("./client/images/*.png").pipe(spritesmith({
    imgName: "sprite.png",
    cssName: "_sprite.scss",
    imgPath: "./client/images/sprite/sprite.png",
    cssFormat: "scss",
    cssVarMap: function (sprite) {
      sprite.name = "sprite-" + sprite.name
    }
  }))

  spriteData.img
    .pipe(gulp.dest("./client/images/sprite/"))

  spriteData.css
    .pipe(gulp.dest("./app/assets/stylesheets/client"))
})
