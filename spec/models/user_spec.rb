require 'rails_helper'

RSpec.describe User, type: :model do
  describe "#uuid" do
    it "Created user hasn't uuid until reload" do
      user = create(:user)
      expect(user.uuid).to be_nil
      user.reload
      expect(user.uuid).not_to be_nil
    end
  end
  describe "email validation" do
    let(:email) {"hogehoge@test.com"}
    before do
      @team = create(:team)
      create(:user, email: email, team: @team)
    end
    context "user who have same email in another team" do
      subject { build(:user, email: email, team: create(:team)) }
      it { is_expected.to be_valid }
    end
    context "user who have same email in team" do
      subject { build(:user, email: email, team: @team) }
      it { is_expected.not_to be_valid }
    end
  end
end
