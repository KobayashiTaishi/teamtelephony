require 'rails_helper'

RSpec.describe Article, type: :model do
  describe "article contents" do
    it "trivia's questions is array" do
      article = create(:article)
      expect(article.contents["objects"][0]["questions"].class).to eq(Array)
    end
  end
end
