# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :phone_number do
    number "MyString"
    sid "MyString"
    team ""
    location "MyString"
    uuid ""
  end
end
