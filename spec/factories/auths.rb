# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auth do
    uid "MyString"
    provider "MyString"
    user_id 1
  end
end
