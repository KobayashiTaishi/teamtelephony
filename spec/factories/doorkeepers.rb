
FactoryGirl.define do

  factory :doorkeeper_application, class: Doorkeeper::Application do
    name { FFaker::Name.first_name }
    uid { FFaker::Internet.user_name }
    secret { FFaker::Internet.user_name }
    redirect_uri { "urn:ietf:wg:oauth:2.0:oob" }
  end
  factory :doorkeeper_access_token do
  end
end
