# Read about factories at https://github.com/thoughtbot/factory_girl

trivia_contents = {
  objects: [
    {type: "trivia",
      results: [
        {text: "", image: nil, description: "", credit: {text: "", url: ""}}
      ],
      questions: [
        {image: nil, text: "", credit: {text: "", url: ""},
          answer_information: {
            column_type: 3,
            answers: [
              {image: nil, text: "", correct: false, credit: {text: "", url: ""}},
              {image: nil, text: "", correct: false, credit: {text: "", url: ""}},
              {image: nil, text: "", correct: false, credit: {text: "", url: ""}}
            ]
          },
          each_result: {explanation: "", image: nil, credit: {text: "", url: ""}}
        }
      ]
    }
  ]
}.to_json
FactoryGirl.define do
  factory :article do
    title "MyString"
    description "MyString"
    sub_url "MyString"
    contents trivia_contents
    theme_id 1
    status 1
    user_id 1
    category
  end
end
