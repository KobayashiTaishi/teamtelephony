# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  password = FFaker::Internet.password(100)

  factory :user do
    email { FFaker::Internet.email }
    name { FFaker::Name.name }
    password { password }
    password_confirmation { password }
    association :team
  end
end
