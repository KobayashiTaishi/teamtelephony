
# include Warden::Test::Helpers
module ExtensionMethodHelper
  def login(email, password)
    post sessions_path, email: email, password: password, remember_me: true
  end
end
