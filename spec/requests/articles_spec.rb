# ローンチまで非公開

# require 'rails_helper'
#
# RSpec.describe "Articles", type: :request do
#   describe "GET /articles" do
#     it "works! (now write some real specs)" do
#       get articles_path
#       expect(response.status).to be(200)
#     end
#   end
#   describe "GET /articles/:id" do
#     it "works!" do
#       article = create(:article)
#       get article_path(article.id)
#       expect(response.status).to be(200)
#     end
#
#     it "tag list" do
#       tag_list = ["TAG", "qolony", "hoge", "大志"]
#       article = create(:article)
#       article.tag_list.add(tag_list)
#       article.save
#       path = "/articles/" + article.id.to_s + ".json"
#       get path
#       json = JSON.parse response.body
#       expect(json["tag_list"]).to match_array(tag_list)
#     end
#
#     it "has references" do
#       references = ["piyo", "hoge"]
#       article = create(:article, references: references)
#       path = "/articles/" + article.id.to_s + ".json"
#       get path
#       json = JSON.parse response.body
#       expect(json["references"]).to eq(references)
#     end
#
#     it "has category name" do
#       category_name = "qolony"
#       category = create(:category, name: category_name)
#       article = create(:article, category: category)
#       path = "/articles/" + article.id.to_s + ".json"
#       get path
#       json = JSON.parse response.body
#       expect(json["category"]).to eq(category_name)
#     end
#
#     it "has published_at formatted to string like 2014.7.8" do
#       article = create(:article, published_at: Time.zone.now)
#       path = "/articles/" + article.id.to_s + ".json"
#       get path
#       json = JSON.parse response.body
#       expect(json["published_at"]).to eq(article.created_at.to_date.strftime("%Y.%m.%d"))
#     end
#   end
#
#   describe "GET /api/p/articles/:id.json" do
#     it "return correct json" do
#       article = create(:article)
#       path = "/api/p/articles/" + article.id.to_s + ".json"
#       get path
#       json = JSON.parse response.body
#       expect(json["contents"]["objects"].first["questions"].class).to eq(Array)
#     end
#   end
# end
