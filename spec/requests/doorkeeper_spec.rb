require 'rails_helper'

describe 'DoorKeeper', type: :request do

  describe 'POST /oauth/token.json' do
    before do
      @password = 'hogehoge'
      @user = create(:user, password: @password, password_confirmation: @password)
      @application = create(:doorkeeper_application)
    end

    describe 'as client_credentials' do
      before do
        post '/oauth/token.json', {
          grant_type: 'client_credentials',
          client_id: @application.uid,
          client_secret: @application.secret,
          redirect_uri: @application.redirect_uri
        }
      end

      it 'return 200 response code' do
        expect(response.status).to eq(200)
      end
    end

    describe 'as password' do
      before do
        post '/oauth/token.json', {
          email: @user.email,
          password: @password,
          grant_type: 'password',
          client_id: @application.uid,
          client_secret: @application.secret,
          redirect_uri: @application.redirect_uri
        }
      end

      it 'return 200 response code' do
        expect(response.status).to eq(200)
      end
    end

    describe 'as authorization_code' do

      # ouatu を認証まえ
      # 未完成
      before do
        login(@user.email, @password)
        get '/oauth/authorize', {
          client_id: @application.uid,
          redirect_uri: @application.redirect_uri,
          response_type: 'code'
        }
        # access_tokenを取ってくる
        # post '/oauth/token.json', {
        #   grant_type: 'authorization_code',
        #   client_id: @application.uid,
        #   client_secret: @application.secret,
        #   redirect_uri: @application.redirect_uri
        # }
      end

      it 'return 200 response code' do
        expect(response.status).to eq(200)
      end
    end
  end
  # it 'memo' do
  # @grant_type = "client_credentials"
  # @user = FactoryGirl.create(:user, password: 'hogehoge', password_confirmation: 'hogehoge')
  # @application = FactoryGirl.create(:doorkeeper_application)
  # @param = "client_id=#{@application.uid}&client_secret=#{@application.secret}&grant_type=#{@grant_type}"
  # response = RestClient.post 'http://localhost:3000/oauth/token', @param
  # end
  #
  # it 'memo' do
  #   password = 'hogehoge'
  #   @grant_type = 'password'
  #   @user = FactoryGirl.create(:user, password: password, password_confirmation: password)
  #   @application = FactoryGirl.create(:doorkeeper_application)
  #   @param = "client_id=#{@application.uid}&client_secret=#{@application.secret}&grant_type=#{@grant_type}&email=#{@user.email}&password=#{password}"
  #   response = RestClient.post 'http://localhost:3000/oauth/token', @param
  #   token = JSON.parse(response)["access_token"]
  #   param = "access_token=#{token}"
  #   response = RestClient.get 'http://localhost:3000/users/7.json', { 'Authorization' => "Bearer #{token}" }

  # end
end
