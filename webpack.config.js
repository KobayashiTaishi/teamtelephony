path = require('path')

var config = {
    entry: {
      app: "./client/src/index.js"
      // worker: "./client/src/domless/worker.js"
    },
    output: {
        path: "./app/assets/javascripts/",
        filename: "client/[name]/[name].bundle.js"
    },
    devtool: "#source-map",
    module: {
        loaders: [
            { test: /\.js$/, loader: "babel-loader", exclude: [/node_modules\/firebase/, /node_modules\/reactfire/, /node_modules\/react-router/], query: {compact: false} },
            { test: /\.jsx$/, loader: "babel-loader", exclude: /node_modules/, query: {compact: false} },
            { test: /\.es6$/, loader: "babel-loader", exclude: /node_modules/, query: {compact: false} }
            // { tes: /\.css$/, loade: "style-loader!css-loader" }
            // { test: /\.scss$/, loader: "style!css!sass?includePaths[]=" + neat + "&includePaths[]=" + neat[0] + "&includePaths[]=" + neat[1]}
        ]
    },
    resolve: {
      alias: {
        // react-draggable-tabのreactのバージョンを本体に合わせる
        'react': path.join(__dirname, 'node_modules', 'react')
      },
      extensions: ["", ".js", ".jsx", ".es6"]
    },
    plugins: []
}

module.exports = config
