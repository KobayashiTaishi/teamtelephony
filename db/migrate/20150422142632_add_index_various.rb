class AddIndexVarious < ActiveRecord::Migration
  def change
    add_index :articles, :theme_id
    add_index :articles, :tags, using: 'gin'
    change_column :articles, :status, :integer, default: 0
  end
end
