class AddColumnThumbnailImageToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :thumbnail_image, :string
  end
end
