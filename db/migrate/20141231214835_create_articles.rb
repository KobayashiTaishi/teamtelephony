class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :description
      t.string :sub_url
      t.integer :theme_id
      t.integer :status
      t.integer :user_id
      t.hstore :contents, array: true, default: []
      t.string :tags, array: true, default: []

      t.timestamps null: false
    end
  end
end
