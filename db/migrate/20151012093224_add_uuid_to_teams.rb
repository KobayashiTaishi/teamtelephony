class AddUuidToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :uuid, :uuid, default: 'uuid_generate_v4()'
  end
end
