class AddReferences < ActiveRecord::Migration
  def change
    remove_index :articles, :tags
    remove_column :articles, :tags
    add_column :articles, :references, :string, array: true, default: []
  end
end
