class AddAttentionToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :attention, :boolean, default: false
  end
end
