class CreatePhoneNumbers < ActiveRecord::Migration
  def change
    create_table :phone_numbers do |t|
      t.string :number
      t.string :sid
      t.references :team
      t.string :location
      t.uuid :uuid, default: 'uuid_generate_v4()'

      t.timestamps null: false
    end
  end
end
