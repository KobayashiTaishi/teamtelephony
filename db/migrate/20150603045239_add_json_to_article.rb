class AddJsonToArticle < ActiveRecord::Migration
  def change
    enable_extension 'citext'
    remove_column :articles, :contents
    add_column :articles, :contents, :jsonb, null: false
    add_index :articles, :contents, using: :gin
  end
end
