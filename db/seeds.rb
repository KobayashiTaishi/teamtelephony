# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

ActiveRecord::Base.transaction do

  case Rails.env
  when "production"
    # プロダクションのみのテストデータ
  when "development"
    # 開発のみのテストデータ
    team = Team.create(name: "Test Team")
    PhoneNumber.create(number: "+81345402452", team_id: team.id)
    User.create(email: "test@campus.com", name: FFaker::Name.name, password: "testtest", password_confirmation: "testtest", status: :normal, team: team)
    User.create!(email: "writer@campus.com", name: FFaker::Name.name, password: "testtest", password_confirmation: "testtest", status: :writer, team: team)
    User.create!(email: "admin@campus.com", name: FFaker::Name.name, password: "testtest", password_confirmation: "testtest", status: :admin, team: team)

    Doorkeeper::Application.create(name: "testtest", uid: "android_uid", secret: "android_secret", redirect_uri: "urn:ietf:wg:oauth:2.0:oob")
  end

  Rails.logger.info "complite"
end
